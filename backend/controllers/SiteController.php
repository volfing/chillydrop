<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\Setting;
use common\models\CaseGroup;
use common\models\CaseCategory;
use common\models\CaseItem;
use common\models\SteamItem;
use common\models\Page;
use common\models\Faq;
use common\models\Feedback;
use common\models\Dayli;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private 
        $upload_dir;//Папка для загрузки


    public function beforeAction($action){
        $this->upload_dir = Yii::getAlias("@app") . "/../frontend/web";

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup', 'about', 'settings'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                       'actions' => ['logout', 'index', 'settings', 'case-groups', 'add-group', 'edit-group', 'delete-group', 'add-case', 'edit-case', 'delete-case', 'cases', 'items', 'add-item', 'edit-item', 'delete-item', 'pages', 'add-page', 'delete-page', 'edit-page', 'faq', 'add-faq', 'edit-faq', 'delete-faq', 'add-feedback', 'edit-feedback', 'delete-feedback', 'feedbacks', 'dayli'],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin(Yii::$app->user->identity->username);
                       }
                   ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDayli(){
        $model = Dayli::findOne(1);
        $cases = CaseItem::find()->all();

        $oldbanner = $model->banner;
        $oldimage = $model->image_of_prize;

        if($model->load(Yii::$app->request->post())){

            $upload_dir = $this->upload_dir;

            if(!empty(UploadedFile::getInstance($model, 'banner'))){
                $model->banner = UploadedFile::getInstance($model, 'banner');//Иконка группы
            }
            
            if(!empty($model->banner)){//Если пришла новая иконка
                $fname = $upload_dir . $model->banner;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->banner->extension;
                $model->banner->saveAs($upload_dir . "/uploads/items/" . $fname);
                $model->banner = "/uploads/items/" . $fname;
            }

            if(empty($model->banner)){
                $model->banner = $oldbanner;
            }

            if(!empty(UploadedFile::getInstance($model, 'image_of_prize'))){
                $model->image_of_prize = UploadedFile::getInstance($model, 'image_of_prize');//Иконка группы
            }
            
            if(!empty($model->image_of_prize)){//Если пришла новая иконка
                $fname = $upload_dir . $model->image_of_prize;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->image_of_prize->extension;
                $model->image_of_prize->saveAs($upload_dir . "/uploads/items/" . $fname);
                $model->image_of_prize = "/uploads/items/" . $fname;
            }

            if(empty($model->image_of_prize)){
                $model->image_of_prize = $oldimage;
            }

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/dayli']);
            }
        }

        return $this->render("dayli", ["model" => $model, "cases" => $cases]);
    }

    public function actionDeleteFeedback($id){//Удаление отзыва 
        $feedback = Feedback::findOne((int)$id);

        if($feedback == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Feedback с таким ID не была найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $feedback->delete();

            return $this->redirect(['site/feedbacks']);
        }

        return $this->render("delete_feedback", ["id" => (int)$id]);   
    }

    /*Список отзывов*/
    public function actionFeedbacks()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find(),
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render("feedbacks", ['dataProvider' => $dataProvider]);
    }

    public function actionEditFeedback($id){//Редактирование отзыва
        $model = Feedback::findOne($id);
        $users = User::find()->all();

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/feedbacks']);
            }
        }

        return $this->render("edit_feedback", ["model" => $model, "users" => $users]);
    }

    public function actionAddFeedback(){//Добавление нового отзыва
        $model = new Feedback();
        $users = User::find()->all();

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/feedbacks']);
            }
        }

        return $this->render("add_feedback", ["model" => $model, "users" => $users]);
    }

    public function actionDeleteFaq($id){//Удаление вопроса 
        $faq = Faq::findOne((int)$id);

        if($faq == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Faq с таким ID не была найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $faq->delete();

            return $this->redirect(['site/faq']);
        }

        return $this->render("delete_faq", ["id" => (int)$id]);   
    }

    public function actionEditFaq($id){//Редактирование вопроса
        $model = Faq::findOne($id);

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/faq']);
            }
        }

        return $this->render("edit_faq", ["model" => $model]);
    }

    public function actionAddFaq(){//Добавление нового вопроса
        $model = new Faq();

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/faq']);
            }
        }

        return $this->render("add_faq", ["model" => $model]);
    }

    public function actionDeletePage($id){//Удаление страницы 
        $page = Page::findOne((int)$id);

        if($page == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Страница с таким ID не была найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $page->delete();

            return $this->redirect(['site/pages']);
        }

        return $this->render("delete_page", ["id" => (int)$id]);   
    }

    /*Список вопросов*/
    public function actionFaq()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Faq::find(),
        ]);

        return $this->render("faq", ['dataProvider' => $dataProvider]);
    }

    public function actionAddPage(){//Добавление новой страницы
        $model = new Page();

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/pages']);
            }
        }

        return $this->render("add_page", ["model" => $model]);
    }

    public function actionEditPage($id){//Редактирование страницы
        $model = Page::findOne($id);

        if($model->load(Yii::$app->request->post())){

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/pages']);
            }
        }

        return $this->render("edit_page", ["model" => $model]);
    }

    /*Список статичных страниц*/
    public function actionPages()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Page::find(),
        ]);

        return $this->render("pages", ['dataProvider' => $dataProvider]);
    }

    public function actionDeleteItem($id){//Удаление кейса 
        $item =  CaseItem::findOne((int)$id);

        if($item == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Кейс с таким ID не был найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $item->delete();

            return $this->redirect(['site/items', "category" => $_GET['category']]);
        }

        return $this->render("delete_item", ["id" => (int)$id]);   
    }

    public function actionEditItem($id, $category){//Редактирование айтема
        $model = CaseItem::findOne((int)$id);

        $steamItems = SteamItem::find()->asArray()->all();

        if($model == NULL){//Если не нашли категорию с таким ид
            return $this->render('edit_group', ['message' => array(
                        'text' => 'Кейс с таким ID не был найдена в системе',
                        'type' => 'danger',
                    )]
            );
        }

        $oldimage = $model->image;

        if($model->load(Yii::$app->request->post())){//Если пришел запрос с формы
            $upload_dir = $this->upload_dir;

            if(!empty(UploadedFile::getInstance($model, 'image'))){
                $model->image = UploadedFile::getInstance($model, 'image');//Иконка группы
            }
            
            if(!empty($model->image)){//Если пришла новая иконка
                $fname = $upload_dir . $model->image;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->image->extension;
                $model->image->saveAs($upload_dir . "/uploads/items/" . $fname);
                $model->image = "/uploads/items/" . $fname;
            }

            if(empty($model->image)){
                $model->image = $oldimage;
            }

            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму
                $model->save();
                return $this->redirect(['site/items', 'category' => $model->category]);
            }
        }

        return $this->render("edit_item", ['model' => $model, "steam_items" => $steamItems]);
    }

    public function actionAddItem(){//Добавление нового айтема
        $model = new CaseItem();

        $steamItems = SteamItem::find()->asArray()->all();

        if($model->load(Yii::$app->request->post())){

            $category = CaseCategory::findOne((int)$model->category);

            if(empty($category)){
                return $this->render("add_item", ["model" => $model, "steam_items" => $steamItems, "message" => array("type" => "danger", "text" => "Выбранной категории кейсов не существует")]);
            }

            $upload_dir = $this->upload_dir . "/uploads/items/";

            $model->image = UploadedFile::getInstance($model, 'image');//Первая фотка

            if($model->image !== NULL){
                $fname1 = time() . rand() . "." . $model->image->extension;
                $model->image->saveAs($upload_dir . $fname1);

                $model->image = "/uploads/items/" . $fname1;
            }

            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/items', 'category' => $model->category]);
            }else{//Если валидация с ошибками, то удаляем файлы с сервера
                unlink($fname1);
            }
        }

        return $this->render("add_item", ["model" => $model, "steam_items" => $steamItems]);
    }

    /*Список айетмов в кейсах*/
    public function actionItems($category)
    {
        if(empty($category)){
            return $this->redirect(['site/case-groups']);
        }

        $category = CaseCategory::findOne((int)$category);

        if(empty($category)){
            return $this->redirect(['site/case-groups']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => CaseItem::find()->where(['category' => $category->id]),
        ]);

        return $this->render("items", ['dataProvider' => $dataProvider]);
    }

    public function actionDeleteCase($id){//Удаление кейса 
        $group =  CaseCategory::findOne((int)$id);

        if($group == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Кейс с таким ID не был найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $group->delete();

            return $this->redirect(['site/cases', "group" => $_GET['group']]);
        }

        return $this->render("delete_case", ["id" => (int)$id]);   
    }

    public function actionEditCase($id, $group){//Редактирование кейса
        $model = CaseCategory::findOne((int)$id);

        if($model == NULL){//Если не нашли категорию с таким ид
            return $this->render('edit_group', ['message' => array(
                        'text' => 'Кейс с таким ID не был найдена в системе',
                        'type' => 'danger',
                    )]
            );
        }

        $oldimage1 = $model->image1;
        $oldimage2 = $model->image2;

        if($model->load(Yii::$app->request->post())){//Если пришел запрос с формы
            $upload_dir = $this->upload_dir;

            if(!empty(UploadedFile::getInstance($model, 'image1'))){
                $model->image1 = UploadedFile::getInstance($model, 'image1');//Иконка группы
            }
            
            if(!empty($model->image1)){//Если пришла новая иконка
                $fname = $upload_dir . $model->image1;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->image1->extension;
                $model->image1->saveAs($upload_dir . "/uploads/groups/" . $fname);
                $model->image1 = "/uploads/groups/" . $fname;
            }

            if(empty($model->image1)){
                $model->image1 = $oldimage1;
            }

            if(!empty(UploadedFile::getInstance($model, 'image2'))){
                $model->image2 = UploadedFile::getInstance($model, 'image2');//Иконка группы
            }
            
            if(!empty($model->image2)){//Если пришла новая иконка
                $fname = $upload_dir . $model->image2;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->image2->extension;
                $model->image2->saveAs($upload_dir . "/uploads/groups/" . $fname);
                $model->image2 = "/uploads/groups/" . $fname;
            }

            if(empty($model->image2)){
                $model->image2 = $oldimage2;
            }


            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму
                $model->save();
                return $this->redirect(['site/cases', 'group' => $model->group]);
            }
        }

        return $this->render("edit_case", ['model' => $model]);
    }

    public function actionAddCase(){//Добавление нового кейса
        $model = new CaseCategory();

        if($model->load(Yii::$app->request->post())){

            $group = CaseGroup::findOne((int)$model->group);

            if(empty($group)){
                return $this->render("add_case", ["model" => $model, "message" => array("type" => "danger", "text" => "Выбранной группы кейсов не существует")]);
            }

            $upload_dir = $this->upload_dir . "/uploads/cases/";

            $model->image1 = UploadedFile::getInstance($model, 'image1');//Первая фотка

            if($model->image1 == NULL){
                return $this->render("add_case", ["model" => $model, "message" => array("type" => "danger", "text" => "Необходимо выбрать первое изображение кейса")]);
            }

            $fname1 = time() . rand() . "." . $model->image1->extension;
            $model->image1->saveAs($upload_dir . $fname1);

            $model->image1 = "/uploads/cases/" . $fname1;

            $model->image2 = UploadedFile::getInstance($model, 'image2');//Первая фотка

            if($model->image2 == NULL){
                return $this->render("add_case", ["model" => $model, "message" => array("type" => "danger", "text" => "Необходимо выбрать второе изображение кейса")]);
            }

            $fname2 = time() . rand() . "." . $model->image2->extension;
            $model->image2->saveAs($upload_dir . $fname2);

            $model->image2 = "/uploads/cases/" . $fname2;

            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/cases', 'group' => $model->group]);
            }else{//Если валидация с ошибками, то удаляем файлы с сервера
                unlink($fname1);
                unlink($fname2);
            }
        }

        return $this->render("add_case", ["model" => $model]);
    }

    /*Список категорий кейсов*/
    public function actionCases($group)
    {
        if(empty($group)){
            return $this->redirect(['site/case-groups']);
        }

        $group = CaseGroup::findOne((int)$group);

        if(empty($group)){
            return $this->redirect(['site/case-groups']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => CaseCategory::find()->where(['group' => $group->id]),
        ]);

        return $this->render("cases", ['dataProvider' => $dataProvider]);
    }

    public function actionDeleteGroup($id){//Удаление группы 
        $group = CaseGroup::findOne((int)$id);

        if($group == null){
            return $this->render(
                "info",
                [
                    "message" => [
                        "type" => "danger",
                        "text" => "Категория с таким ID не была найдена в системе"
                    ]
                ] 
            );
        }

        if(Yii::$app->request->get("delete") != null){//Значит удаление подтверждено
            $group->delete();

            return $this->redirect(['site/case-groups']);
        }

        return $this->render("delete_group", ["id" => (int)$id]);   
    }


    public function actionEditGroup($id){//Редактирование группы
        $model = CaseGroup::findOne((int)$id);

        if($model == NULL){//Если не нашли категорию с таким ид
            return $this->render('edit_group', ['message' => array(
                        'text' => 'Категория с таким ID не была найдена в системе',
                        'type' => 'danger',
                    )]
            );
        }

        $oldicon = $model->icon;
        $oldbg = $model->background;

        if($model->load(Yii::$app->request->post())){//Если пришел запрос с формы
            $upload_dir = $this->upload_dir;

            if(!empty(UploadedFile::getInstance($model, 'icon'))){
                $model->icon = UploadedFile::getInstance($model, 'icon');//Иконка группы
            }
            
            if(!empty($model->icon)){//Если пришла новая иконка
                $fname = $upload_dir . $model->icon;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->icon->extension;
                $model->icon->saveAs($upload_dir . "/uploads/groups/" . $fname);
                $model->icon = "/uploads/groups/" . $fname;
            }

            if(empty($model->icon)){
                $model->icon = $oldicon;
            }

            if(!empty(UploadedFile::getInstance($model, 'background'))){
                $model->background = UploadedFile::getInstance($model, 'background');//Иконка группы
            }
            
            if(!empty($model->background)){//Если пришла новая иконка
                $fname = $upload_dir . $model->background;
                @unlink($fname);//Удаляем текущую иконку
                $fname = time() . rand() . "." . $model->background->extension;
                $model->background->saveAs($upload_dir . "/uploads/groups/" . $fname);
                $model->background = "/uploads/groups/" . $fname;
            }

            if(empty($model->background)){
                $model->background = $oldbg;
            }

            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму
                $model->save();
                return $this->redirect(['site/case-groups']);
            }
        }

        return $this->render("edit_group", ['model' => $model]);
    }


    public function actionAddGroup(){//Добавление новой группы
        $model = new CaseGroup();

        if($model->load(Yii::$app->request->post())){

            $upload_dir = $this->upload_dir . "/uploads/groups/";

            $model->icon = UploadedFile::getInstance($model, 'icon');//Иконка группы

            if($model->icon == NULL){
                return $this->render("add_group", ["model" => $model, "message" => array("type" => "danger", "text" => "Необходимо выбрать иконку группы")]);
            }

            $fname1 = time() . rand() . "." . $model->icon->extension;
            $model->icon->saveAs($upload_dir . $fname1);

            $model->icon = "/uploads/groups/" . $fname1;

            $model->background = UploadedFile::getInstance($model, 'background');//Фоновоая картинка

            if(!empty($model->background)){
                $fname2 = time() . rand() . "." . $model->background->extension;
                $model->background->saveAs($upload_dir . $fname2);

                $model->background = "/uploads/groups/" . $fname2;
            }

            if(empty($model->sort)){
                $model->sort = 1;
            }

            if($model->validate()){//Проверяем форму и сохраняем ее, если все ОК
                $model->save();
                return $this->redirect(['site/case-groups']);
            }else{//Если валидация с ошибками, то удаляем файлы с сервера
                unlink($fname1);

                if(!empty($fname2)){
                    unlink($fname2);
                }
            }
        }

        return $this->render("add_group", ["model" => $model]);
    }

    /*Список групп для кейсов*/
    public function actionCaseGroups()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CaseGroup::find(),
        ]);

        return $this->render("case_groups", ['dataProvider' => $dataProvider]);
    }

    /*Список настроек*/
    public function actionSettings()
    {
        $post = Yii::$app->request->post();

        if(isset($post["save"])){
            Setting::saveSettings($post);
        }

        $settings = Setting::find()->asArray()->all();

        return $this->render('settings', ["settings" => $settings]);
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['site/case-groups']);
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
