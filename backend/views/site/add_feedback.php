<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use moonland\tinymce\TinyMCE;

$items = array();

foreach ($users as $key => $value) {
	$items[$value["id"]] = $value["login"];
}

$this->title = "Добавление нового отзыва";

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		    <?= $form->field($model, 'text')->widget(TinyMCE::className()); ?>

		    <?= $form->field($model, 'eng_text')->widget(TinyMCE::className()); ?>

		    <?=$form->field($model, 'user_id')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите пользователя ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);

		    ?>

		    <?= $form->field($model, 'active')->checkbox() ?>

		    <div class="form-group">
		        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}

		if(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message["type"]?>"><?=$message["text"]?></p>
		<?
		}

		?>
	</div>
</div>

</div>
