<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = "Список отзывов";
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            	'attribute' => "active",
            	'content' => function($data){
            		return $data->active == 1 ? "Да" : "Нет";
            	}
            ],
            [
                'attribute' => 'text',
                'content' => function($data){
                    return mb_strimwidth(strip_tags($data->text), 0, 110) . "...";
                }
            ],
            [
            	'class' => 'yii\grid\ActionColumn',
	            'buttons' => [
	            	'view' => function($url, $model){
	            		return '';
	            	},
	            	'update' => function($url, $model){
	            		return '<a href="/index.php?r=site/edit-feedback&id=' . $model->id . '" title="Изменить" aria-label="Изменить"><span class="glyphicon glyphicon-pencil"></span> изменить</a><br>';
	            	},
	            	'delete' => function($url, $model){
	            		return '<a href="/index.php?r=site/delete-feedback&id=' . $model->id . '" title="Удалить" aria-label="Удалить"><span class="glyphicon glyphicon-trash"></span> удалить</a><br>';
	            	}
	            ]
            ],
        ],
    ]); ?>

	<a href="/index.php?r=site/add-feedback" class="btn btn-success">Добавить новый отзыв</a>
</div>

</div>
