<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\SteamItem;

$this->title = "Ежедневный приз";

$items = array();

foreach ($cases as $key => $value) {
	if(empty($value->title)){
		$steam_item = SteamItem::findOne($value->item);
		$value->title = $steam_item->name;
	}

	$items[$value->id] = $value->title;
}

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

			<?= $form->field($model, 'item1')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите 1-й айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);
		    ?>

		    <?= $form->field($model, 'item2')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите 2-й айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);
		    ?>

		    <?= $form->field($model, 'item3')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите 3-й айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);
		    ?>

		    <?= $form->field($model, 'item4')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите 4-й айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);
		    ?>

		    <?= $form->field($model, 'item5')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите 5-й айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);
		    ?> 

		    <?= $form->field($model, 'name_of_prize') ?>

		    <?= $form->field($model, 'eng_name_of_prize') ?>

		    <img style="max-width:100%;margin-bottom:10px;" src="http://<?=str_replace('admin.', '', $_SERVER['HTTP_HOST'])?>/<?=$model->banner?>" alt="">

		    <?= $form->field($model, 'banner')->fileInput() ?>

		    <img style="max-width:100%;margin-bottom:10px;margin-top:10px;" src="http://<?=str_replace('admin.', '', $_SERVER['HTTP_HOST'])?>/<?=$model->image_of_prize?>" alt="">

		    <?= $form->field($model, 'image_of_prize')->fileInput() ?>

		    <?= $form->field($model, 'min_prize') ?>

		    <?= $form->field($model, 'max_prize') ?>

		    <?= $form->field($model, 'min_payment') ?>

		    <div class="form-group">
		        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}

		if(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message["type"]?>"><?=$message["text"]?></p>
		<?
		}

		?>
	</div>
</div>

</div>
