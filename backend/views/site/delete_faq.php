<?php

$this->title = "Удаление вопроса";

?>

<div class="container">
	<h1><?=$this->title?></h1>

	<?
	if(isset($message)){
	?>
	<p style="padding: 10px;" class="bg-<?=$message['type']?>"><?=$message["text"]?></p>
	<?
	}else{
	?>

	<p>Вы уверены, что хотите удалить вопрос #<?=$id?> ?</p>

	<a class="btn btn-danger" href="/index.php?r=site/delete-faq&id=<?=$id?>&delete=1">Да</a>
	<a class="btn btn-default" href="javascript://" onclick="history.go(-1);">Нет</a>

	<?
	}
	?>

	

</div>
