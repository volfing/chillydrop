<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use moonland\tinymce\TinyMCE;

$this->title = "Редактирование вопроса";

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		    <?= $form->field($model, 'title') ?>

		    <?= $form->field($model, 'title_eng') ?>

		    <?= $form->field($model, 'answer')->widget(TinyMCE::className()); ?>

		    <?= $form->field($model, 'answer_eng')->widget(TinyMCE::className()); ?>

		    <div class="form-group">
		        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}

		if(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message["type"]?>"><?=$message["text"]?></p>
		<?
		}

		?>
	</div>
</div>

</div>
