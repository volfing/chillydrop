<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Редактирование кейса";

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		    <?= $form->field($model, 'title') ?>

		    <?= $form->field($model, 'eng_title') ?>

		    <?= $form->field($model, 'price') ?>

		    <?= $form->field($model, 'image1')->fileInput() ?>

		    <?= $form->field($model, 'image2')->fileInput() ?>

		    <?= $form->field($model, 'sort') ?>

		    <?= $form->field($model, 'active')->checkbox() ?>
			
			<div style="display: none;">
				<?= $form->field($model, 'group')->hiddenInput(['value' => $_GET['group']]) ?>
			</div>

		    <div class="form-group">
		        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}

		if(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message["type"]?>"><?=$message["text"]?></p>
		<?
		}

		?>
	</div>
</div>

</div>
