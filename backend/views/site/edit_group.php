<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Редактирование группы";

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		    <?= $form->field($model, 'title') ?>

		    <?= $form->field($model, 'eng_title') ?>

		    <?= $form->field($model, 'icon')->fileInput() ?>

		    <?= $form->field($model, 'background')->fileInput() ?>

		    <?= $form->field($model, 'sort') ?>

		    <?= $form->field($model, 'active')->checkbox() ?>

		    <div class="form-group">
		        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}elseif(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message['type']?>"><?=$message['text']?></p>
		<?
		}

		?>
	</div>
</div>

</div>
