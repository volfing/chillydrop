<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\SteamItem;

$this->title = "Список вещей";
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
               'attribute' => "title",
                'content' => function($data){
                    if(!empty($data->title)){
                        return $data->title;
                    }else{
                        $item = SteamItem::findOne($data->item);

                        return $item->name;
                    }
                } 
            ],
            [
               'attribute' => "image",
                'content' => function($data){
                    if(!empty($data->image)){
                        return '<img src="http://' . str_replace('admin.', '', $_SERVER['HTTP_HOST']) . '/' . $data->image . '" style="width:100px;">';
                    }else{
                        $item = SteamItem::findOne($data->item);

                        return '<img src="https://' . $item->image . '" style="width:100px;">';
                    }
                } 
            ],
            'chance:ntext',
            [
            'attribute' => "active",
                'content' => function($data){
                    return $data->active == 1 ? "Да" : "Нет";
                }
            ],
            'sort:ntext',
            [
            	'class' => 'yii\grid\ActionColumn',
	            'buttons' => [
                    'view' => function($data){
                        return '';
                    },
	            	'update' => function($url, $model){
	            		return '<a href="/index.php?r=site/edit-item&id=' . $model->id . '&category=' . $model->category . '" title="Изменить" aria-label="Изменить"><span class="glyphicon glyphicon-pencil"></span> изменить</a><br>';
	            	},
	            	'delete' => function($url, $model){
	            		return '<a href="/index.php?r=site/delete-item&id=' . $model->id . '&category=' . $model->category . '" title="Удалить" aria-label="Удалить"><span class="glyphicon glyphicon-trash"></span> удалить</a><br>';
	            	}
	            ]
            ],
        ],
    ]); ?>

	<a href="/index.php?r=site/add-item&category=<?=$_GET['category']?>" class="btn btn-success">Добавить новый айтем</a>
</div>

</div>
