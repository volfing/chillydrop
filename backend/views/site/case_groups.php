<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = "Список групп для кейсов";
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title:ntext',
            [
            	'attribute' => "active",
            	'content' => function($data){
            		return $data->active == 1 ? "Да" : "Нет";
            	}
            ],
            [
            	'class' => 'yii\grid\ActionColumn',
	            'buttons' => [
	            	'view' => function($url, $model){
	            		return '<a href="/index.php?r=site/cases&group=' . $model->id . '" title="Список кейсов" aria-label="Список кейсов"><span class="glyphicon glyphicon-eye-open"></span> список кейсов</a><br>';
	            	},
	            	'update' => function($url, $model){
	            		return '<a href="/index.php?r=site/edit-group&id=' . $model->id . '" title="Изменить" aria-label="Изменить"><span class="glyphicon glyphicon-pencil"></span> изменить</a><br>';
	            	},
	            	'delete' => function($url, $model){
	            		return '<a href="/index.php?r=site/delete-group&id=' . $model->id . '" title="Удалить" aria-label="Удалить"><span class="glyphicon glyphicon-trash"></span> удалить</a><br>';
	            	}
	            ]
            ],
        ],
    ]); ?>

	<a href="/index.php?r=site/add-group" class="btn btn-success">Добавить новую группу</a>
</div>

</div>
