<?php

$this->title = "Удаление кейса";

?>

<div class="container">
	<h1><?=$this->title?></h1>

	<?
	if(isset($message)){
	?>
	<p style="padding: 10px;" class="bg-<?=$message['type']?>"><?=$message["text"]?></p>
	<?
	}else{
	?>

	<p>Вы уверены, что хотите удалить кейс #<?=$id?> ?</p>

	<a class="btn btn-danger" href="/index.php?r=site/delete-case&id=<?=$id?>&delete=1&group=<?=$_GET['group']?>">Да</a>
	<a class="btn btn-default" href="javascript://" onclick="history.go(-1);">Нет</a>

	<?
	}
	?>

	

</div>
