<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = "Добавление нового айтема";

$items = array();

foreach ($steam_items as $key => $value) {
	$items[$value["id"]] = $value["name"] . " (Цена: {$value['price']}руб.)";
}

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>


	<div class="col-sm-12">
		<?php 
		if(isset($model)){

		$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		    <?= $form->field($model, 'title') ?>

		    <?= $form->field($model, 'eng_title') ?>

		    <?= $form->field($model, 'item')->widget(Select2::classname(), [
			    'data' => $items,
			    'language' => 'ru',
			    'options' => ['placeholder' => 'Выберите айтем ...'],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			]);

		    ?>
		    
		    <?= $form->field($model, 'chance') ?>

		    <?= $form->field($model, 'image')->fileInput() ?>

		    <?= $form->field($model, 'sort') ?>

		    <?= $form->field($model, 'active')->checkbox() ?>

		    <div style="display: none;">
				<?= $form->field($model, 'category')->hiddenInput(['value' => $_GET['category']]) ?>
			</div>

		    <div class="form-group">
		        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
		    </div>

		<?php 

		ActiveForm::end(); 

		}

		if(isset($message)){
		?>
		<p style="padding: 10px;" class="bg-<?=$message["type"]?>"><?=$message["text"]?></p>
		<?
		}

		?>
	</div>
</div>

</div>
