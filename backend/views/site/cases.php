<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = "Список кейсов";
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-index container">


<div class="row">
	<h1><?=$this->title?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title:ntext',
            [
               'attribute' => "image1",
                'content' => function($data){
                    return '<img src="http://' . str_replace('admin.', '', $_SERVER['HTTP_HOST']) . '/' . $data->image1 . '" style="width:100px;">';
                } 
            ],
            [
               'attribute' => "image2",
                'content' => function($data){
                    return '<img src="http://' . str_replace('admin.', '', $_SERVER['HTTP_HOST']) . '/' . $data->image2 . '" style="width:100px;">';
                } 
            ],
            'price:ntext',
            [
            	'attribute' => "active",
            	'content' => function($data){
            		return $data->active == 1 ? "Да" : "Нет";
            	}
            ],
            'sort:ntext',
            [
            	'class' => 'yii\grid\ActionColumn',
	            'buttons' => [
	            	'view' => function($url, $model){
	            		return '<a href="/index.php?r=site/items&category=' . $model->id . '" title="Список вещей" aria-label="Список вещей"><span class="glyphicon glyphicon-eye-open"></span> список вещей</a><br>';
	            	},
	            	'update' => function($url, $model){
	            		return '<a href="/index.php?r=site/edit-case&id=' . $model->id . '&group=' . $model->group . '" title="Изменить" aria-label="Изменить"><span class="glyphicon glyphicon-pencil"></span> изменить</a><br>';
	            	},
	            	'delete' => function($url, $model){
	            		return '<a href="/index.php?r=site/delete-case&id=' . $model->id . '&group=' . $model->group . '" title="Удалить" aria-label="Удалить"><span class="glyphicon glyphicon-trash"></span> удалить</a><br>';
	            	}
	            ]
            ],
        ],
    ]); ?>

	<a href="/index.php?r=site/add-case&group=<?=$_GET['group']?>" class="btn btn-success">Добавить новый кейс</a>
</div>

</div>
