<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Настройки';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="bs-example"> 
                	<?
                	$form = ActiveForm::begin([
					    'id' => 'settings-form',
					    'options' => [],
					    'method' => 'post'
					]) ?>
					    
					<?foreach ($settings as $key => $value):?>
                    	<div class="form-group col-lg-12"> 
                            <label for="param_<?=$value['id']?>"><?=$value['title']?></label> 
                            <input type="<?=$value['type']?>" name="param_<?=$value['id']?>" class="form-control" id="param_<?=$value['id']?>" value="<?=str_replace('"', "'", $value['value'])?>"> 
                        </div> 
                    <?endforeach;?>
                    <input type="hidden" name="save" value="1">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-default">Сохранить настройки</button> 
                    </div>
					   
					<?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
