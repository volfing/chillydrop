<?php

namespace common\models;

use Yii;
use common\models\Settings;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $summ
 * @property integer $success
 * @property string $date
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'summ', 'date'], 'required'],
            [['user_id', 'success'], 'integer'],
            [['summ'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'summ' => 'Summ',
            'success' => 'Success',
            'date' => 'Date',
        ];
    }

    public function generateForm(){
        if(empty($this->id)){
            $this->save();
        }
        
        $WMI_MERCHANT_ID = Setting::findByKey('WMI_MERCHANT_ID');
        $WMI_SUCCESS_URL = Setting::findByKey('payment_success_link');
        $WMI_FAIL_URL = Setting::findByKey('payment_error_link');
        $key = Setting::findByKey('walletone_secret');
        $key = $key->value;
        $WMI_PAYMENT_NO = $this->id;
        // Добавление полей формы в ассоциативный массив
        $fields["WMI_MERCHANT_ID"]    = $WMI_MERCHANT_ID->value;
        $fields["WMI_PAYMENT_AMOUNT"] = number_format((float)$this->summ, 2);
        $fields["WMI_CURRENCY_ID"]    = "643";
        $fields["WMI_PAYMENT_NO"]     = $WMI_PAYMENT_NO;
        $fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode("Пополнение счета '#" . $this->user_id . "' на " . $_SERVER["SERVER_NAME"]);
        $fields["WMI_EXPIRED_DATE"]   = date("Y-m-d", strtotime(' +1 day')) . "T23:59:59";
        $fields["WMI_SUCCESS_URL"]    = $WMI_SUCCESS_URL->value;
        $fields["WMI_FAIL_URL"]       = $WMI_FAIL_URL->value;
        $fields["USER_ID"]            = $this->user_id;
        /*$fields["MyShopParam3"]       = "Value3"; // при формировании подписи!*/
        //Если требуется задать только определенные способы оплаты, раскоментируйте данную строку и перечислите требуемые способы оплаты.
        //$fields["WMI_PTENABLED"]      = array("UnistreamRUB", "SberbankRUB", "RussianPostRUB");
     
        //Сортировка значений внутри полей
        foreach($fields as $name => $val) 
        {
            if(is_array($val))
            {
                usort($val, "strcasecmp");
                $fields[$name] = $val;
            }
        }
     
        // Формирование сообщения, путем объединения значений формы, 
        // отсортированных по именам ключей в порядке возрастания.
        uksort($fields, "strcasecmp");
        $fieldValues = "";
     
        foreach($fields as $value) 
        {
            if(is_array($value))
                foreach($value as $v)
                {
                    //Конвертация из текущей кодировки (UTF-8)
                    //необходима только если кодировка магазина отлична от Windows-1251
                    $v = iconv("utf-8", "windows-1251", $v);
                    $fieldValues .= $v;
                }
            else
            {
                //Конвертация из текущей кодировки (UTF-8)
                //необходима только если кодировка магазина отлична от Windows-1251
                $value = iconv("utf-8", "windows-1251", $value);
                $fieldValues .= $value;
            }
        }
     
        // Формирование значения параметра WMI_SIGNATURE, путем 
        // вычисления отпечатка, сформированного выше сообщения, 
        // по алгоритму MD5 и представление его в Base64
     
        $signature = base64_encode(pack("H*", md5($fieldValues . $key)));
     
        //Добавление параметра WMI_SIGNATURE в словарь параметров формы
     
        $fields["WMI_SIGNATURE"] = $signature;
     
        // Формирование HTML-кода платежной формы
     
        $content = "<form id='payment' style='display:none;' action='https://wl.walletone.com/checkout/checkout/Index' method='POST'>";
     
        foreach($fields as $key => $val)
        {
            if(is_array($val))
                foreach($val as $value)
                {
                    $content .= "$key: <input type='text' name='$key' value='$value'/>";
                }
            else     
                $content .= "$key: <input type='text' name='$key' value='$val'/>";
        }
     
        $content .= "<input type='submit'/></form>";

        return $content;
    }
}
