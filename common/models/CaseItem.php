<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "case_item".
 *
 * @property integer $id
 * @property string $title
 * @property double $chance
 * @property string $image
 * @property string $steam_id
 * @property integer $category
 * @property integer $active
 * @property integer $sort
 */
class CaseItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'case_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chance', 'item', 'category'], 'required'],
            [['chance'], 'number'],
            [['category', 'active', 'sort', 'item'], 'integer'],
            [['title', 'eng_title'], 'string', 'max' => 200],
            [['image'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'eng_title' => 'Название на английском',
            'chance' => 'Шанс выигрыша',
            'image' => 'Изображение',
            'item' => 'Айтем',
            'category' => 'Кейс',
            'active' => 'Включен',
            'sort' => 'Индекс сортировки',
        ];
    }
}
