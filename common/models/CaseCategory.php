<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "case_category".
 *
 * @property integer $id
 * @property integer $price
 * @property string $title
 * @property string $image1
 * @property string $image2
 * @property integer $active
 * @property integer $sort
 * @property integer $group
 */
class CaseCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'case_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'title', 'eng_title', 'group'], 'required'],
            [['price', 'active', 'sort', 'group'], 'integer'],
            [['title', 'eng_title'], 'string', 'max' => 100],
            [['image1', 'image2'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Цена открытия',
            'title' => 'Название',
            'eng_title' => 'Название на английском',
            'image1' => 'Изображение 1',
            'image2' => 'Изображение 2',
            'active' => 'Включен',
            'sort' => 'Индекс сортировки',
            'group' => 'Group',
        ];
    }
}
