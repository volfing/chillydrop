<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property integer $active
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'text', 'eng_text'], 'required'],
            [['user_id', 'active'], 'integer'],
            [['text', 'eng_text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'text' => 'Текст отзыва',
            'eng_text' => 'Текст отзыва на английском',
            'active' => 'Отзыв опубликован',
        ];
    }
}
