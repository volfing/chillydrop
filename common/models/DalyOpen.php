<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daly_open".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $summ_of_prize
 * @property string $date
 */
class DalyOpen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daly_open';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'summ_of_prize'], 'required'],
            [['user_id', 'summ_of_prize'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'summ_of_prize' => 'Summ Of Prize',
            'date' => 'Date',
        ];
    }
}
