<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $title
 * @property string $title_eng
 * @property string $answer
 * @property string $answer_eng
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'title_eng', 'answer', 'answer_eng'], 'required'],
            [['title', 'title_eng'], 'string', 'max' => 100],
            [['answer', 'answer_eng'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Вопрос',
            'title_eng' => 'Вопрос на английском',
            'answer' => 'Ответ',
            'answer_eng' => 'Ответ на английском',
        ];
    }
}
