<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dayli".
 *
 * @property integer $id
 * @property integer $item1
 * @property integer $item2
 * @property integer $item3
 * @property integer $item4
 * @property integer $item5
 * @property integer $max_prize
 * @property integer $min_prize
 * @property integer $min_payment
 * @property string $banner
 * @property string $name_of_prize
 * @property string $image_of_prize
 */
class Dayli extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dayli';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item1', 'item2', 'item3', 'item4', 'item5', 'max_prize', 'min_prize', 'min_payment', 'name_of_prize', 'eng_name_of_prize'], 'required'],
            [['item1', 'item2', 'item3', 'item4', 'item5', 'max_prize', 'min_prize', 'min_payment'], 'integer'],
            [['banner', 'name_of_prize', 'image_of_prize'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item1' => 'Первый выигрышный айтем',
            'item2' => 'Второй выигрышный айтем',
            'item3' => 'Третий выигрышный айтем',
            'item4' => 'Четвертый выигрышный айтем',
            'item5' => 'Пятый выигрышный айтем',
            'max_prize' => 'Максимальный возможной приз (в рублях)',
            'min_prize' => 'Минимальный возможный приз (в рублях)',
            'min_payment' => 'Минимальная сумма платежей для участия в розыгрыше',
            'banner' => 'Баннер',
            'name_of_prize' => 'Название приза',
            'eng_name_of_prize' => 'Название приза на английском',
            'image_of_prize' => 'Изображение приза',
        ];
    }
}
