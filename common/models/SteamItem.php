<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "steam_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $hash_name
 * @property string $image
 * @property string $color
 * @property double $price
 */
class SteamItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'steam_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'hash_name', 'image', 'color', 'price'], 'required'],
            [['price'], 'number'],
            [['name', 'hash_name'], 'string', 'max' => 300],
            [['image'], 'string', 'max' => 400],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'hash_name' => 'Hash Name',
            'image' => 'Image',
            'color' => 'Color',
            'price' => 'Price',
        ];
    }
}
