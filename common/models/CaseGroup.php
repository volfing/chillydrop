<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "case_group".
 *
 * @property integer $id
 * @property string $title
 * @property string $icon
 * @property integer $sort
 */
class CaseGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'case_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'eng_title'], 'required'],
            [['sort', 'active'], 'integer'],
            [['title', 'eng_title'], 'string', 'max' => 200],
            [['icon', 'background'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'icon' => 'Иконка',
            'sort' => 'Индекс сортировки',
            'active' => 'Включена',
            'background' => 'Фоновое изображение',
            'eng_title' => 'Название на английском'
        ];
    }
}
