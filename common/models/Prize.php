<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prize".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $sended
 */
class Prize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prize';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id'], 'required'],
            [['user_id', 'item_id', 'sended'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
            'sended' => 'Sended',
        ];
    }
}
