<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $value
 * @property string $type
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title'], 'required'],
            [['value'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 200],
            [['type'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'value' => 'Value',
            'type' => 'Type',
        ];
    }

    public static function findByKey($key, $asArray = false){
        if($asArray){
            return Setting::find()->where(['name' => $key])->asArray()->one();
        }

        return Setting::find()->where(['name' => $key])->one();
    }

    public static function saveSettings($data){
        foreach ($data as $key => $value) {
            $key = explode("param_", $key);

            if(count($key) != 2 || (int)$key[1] < 1){
                continue;
            }

            $setting = Setting::findOne((int)$key[1]);
            $setting->value = $value;
            $setting->save();
        }
    }
}
