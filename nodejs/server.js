var winston    = require('winston');
var config = require('./config.json');
var server = require('http').createServer();
var io = require('socket.io')(server);
var mysql = require('mysql');

var db_config = {//Данные для подключения к БД
	host: config.mysql_host,
	user: config.mysql_user,
	password: config.mysql_password,
	database: config.mysql_db
};

winston.configure({
	transports: [
		new (winston.transports.Console)(),
		new (winston.transports.File)({ filename: 'server.log' })
	]
});

var db = mysql.createConnection(db_config);

db.query("UPDATE stat SET value=0 WHERE type='online'");

io.on("connection", function(socket){
	db.query("UPDATE stat SET value=value+1 WHERE type='online'");

	socket.on("disconnect", () => {
		db.query("UPDATE stat SET value=value-1 WHERE type='online'");
	});

	socket.on("open_case", (data) => {
		if(!data.id || !parseInt(data.id)){
			return;
		}

		newWinner(data);
		
	});
});

function newWinner(data){
	db.query("SELECT * FROM prize ORDER BY id DESC LIMIT 20", (err, rows) => {
		if(err){
			winston.log('error', err);
			return;
		}

		if(!rows.length){
			return;
		}

		rows.forEach((prize) => {
			if(prize.id == parseInt(data.id)){
				db.query("SELECT * FROM user WHERE id=" + prize.user_id, (err, user) => {
					if(err){
						winston.log('error', err);
						return;
					}

					if(!user.length){
						return;
					}

					db.query("SELECT * FROM case_item WHERE id=" + prize.item_id, (err, caseItem) => {
						if(err){
							winston.log('error', err);
							return;
						}

						if(!caseItem.length){
							return;
						}

						db.query("SELECT * FROM steam_item WHERE id=" + caseItem[0].item, (err, steamItem) => {
							if(err){
								winston.log('error', err);
								return;
							}

							if(!steamItem.length){
								return;
							}

							var title = steamItem[0].name;
							var photo = steamItem[0].image;

							if(caseItem[0].image){
								photo = caseItem[0].image;
							}

							if(data.lang != "RU"){
								if(caseItem[0].eng_title){
									title = caseItem[0].eng_title;
								}
							}else{
								if(caseItem.title){
									title = caseItem[0].title;
								}
							}

							io.emit("new_prize", {title: title, user_photo: user[0].photo, photo: photo, color: steamItem[0].color});
						});
					});
				});
				return;
			}
		});
	});
}

setInterval(() => {
	db.query("SELECT * FROM stat WHERE type='online'", (err, stat) => {
		if(err){
			winston.log('error', err);
			return;
		}

		if(!stat.length){
			return;
		}

		db.query("SELECT * FROM setting WHERE name='online_plus'", (err, online_plus) => {
			if(err){
				winston.log('error', err);
				return;
			}

			db.query("SELECT * FROM stat WHERE type='fake_online'", (err, fake_online) => {
				var plus_fake_online = 0;

				if(fake_online.length){
					plus_fake_online = parseInt(fake_online[0].value) + getRandomInt(-10, 10);

					if(plus_fake_online < -100){
						plus_fake_online = -100;
					}else if(plus_fake_online > 100){
						plus_fake_online = 100;
					}
				}

				if(online_plus.length){
					io.emit("online_stat", {online: (parseInt(stat[0].value) + parseInt(online_plus[0].value) + parseInt(plus_fake_online))});
				}else{
					io.emit("online_stat", {online: (parseInt(stat[0].value) + parseInt(plus_fake_online))});
				}

				db.query("UPDATE stat SET value='" + plus_fake_online + "' WHERE type='fake_online'");
			});

			
		});

	});
	
}, 60000);

function fakeWinner(){
	db.query("SELECT * FROM case_item WHERE active=1 ORDER BY RAND() LIMIT 1", (err, item) => {
		if(err){
			winston.log('error', err);
			return;
		}

		if(!item.length){
			return;
		}

		db.query("SELECT * FROM user WHERE is_real=0 ORDER BY RAND() LIMIT 1", (err, user) => {
			if(err){
				winston.log('error', err);
				return;
			}

			if(!user.length){
				return;
			}

			db.query("INSERT INTO prize(user_id, item_id, sended, wait, is_fake) VALUES(" + user[0].id + ", " + item[0].id + ", 0, 0, 1)", (err, res) => {
				if(err){
					winston.log("error", err);
					return;
				}

				if(res.insertId){
					db.query("UPDATE stat SET value=value+1 WHERE type='case_opened'");
					newWinner({id: res.insertId});
				}
			});
		});
	});

	db.query("SELECT * FROM setting WHERE name='time_to_fake_prize'", (err, res) => {
		if(err){
			winston.log('error', err);
			setTimeout(() => {
				fakeWinner();
			}, 10000);
		}

		var time = getRandomInt(1, parseInt(res[0].value));

		setTimeout(() => {
			fakeWinner();
		}, time*1000);
	});
}

// использование Math.round() даст неравномерное распределение!
function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function changeUnique(){
	db.query("UPDATE stat SET value=value+1 WHERE type='unique_players'", () => {
		io.emit('unique_players_changed');

		var nextChange = getRandomInt(0, 300);

		setTimeout(() => {
			changeUnique();
		}, nextChange*1000);
	});
}

setTimeout(() => {
	changeUnique();
}, 10000);

function changeQuantity(){
	db.query("SELECT * FROM setting WHERE name='drop_quantity'", (err, rows) => {
		if(err){
			winston.log('error', err);
			return;
		}

		if(!rows.length){
			return;
		}

		var quantity = getRandomInt(78, 85);

		db.query("UPDATE setting SET value='" + quantity + "' WHERE name='drop_quantity'", () => {
			io.emit('quantity_changed', {quantity: quantity});

			var nextChange = getRandomInt(10, 180);

			setTimeout(() => {
				changeQuantity();
			}, nextChange*1000);
		});
	});
}

setTimeout(() => {
	changeQuantity();
}, 10000);

setTimeout(() => {
	fakeWinner();
}, 10000);

setInterval(() => {
	db.query("SELECT * FROM prize WHERE is_fake=1 ORDER BY ID ASC LIMIT 60", (err, items) => {
		if(err){
			winston.log('error', err);
			return;
		}

		if(items.length < 60){
			return;
		}

		var itemsForDelete = [];

		items.forEach((item, index, array) => {
			if(itemsForDelete.length < 30){
				itemsForDelete[itemsForDelete.length] = item.id;
			}

			if(index == array.length - 1){
				db.query("DELETE FROM prize WHERE id IN(" + (itemsForDelete.join(",")) + ")");
			}
		});
	});
}, 180000);

io.listen(config.io_port);

setInterval(function(){
	db.query("SELECT 1");
}, 5000);

