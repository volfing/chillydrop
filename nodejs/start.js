var SteamCommunity = require('steamcommunity');
var SteamTotp = require('steam-totp');
var config = require('./config.json');
var mysql  = require('mysql');
var SteamTradeOffers = require('steam-tradeoffers');
var request = require('request');

var db_config = {//Данные для подключения к БД
	host: 'localhost',
	user: 'admin_csgo',
	password: '7WwRCFuRPi',
	database: 'admin_csgo'
};

var steamAccountSettings = {//Данные стим аккаунта (берутся из БД)
	"accountName": "binyhe",
	"password": "2356890399b",
	"sharedSecret": "uci2QO7ufTaoHTRGi9H9HH5RjvE=",
	"identitySecret": "IXf5loTGepuFhFE1nmmrXPvQf/w=",
	"twoFactorCode": SteamTotp.generateAuthCode("MAQZSodj+Zk9184QW1XT2GVq234=")
};

var steamLoginData = {//Данные, которые приходят после авторизации
	"sessionID": "",
	"cookies": "",
	"steamguard": "",
	"oAuthToken": "",
	"apiKey": ""
};

var db = mysql.createConnection(db_config);

setInterval(function(){
	db.query("SELECT 1");
}, 5000);

var adminsID = getAdminsID();//Steam ID админов

//console.log(SteamTotp.generateAuthCode("YX8+C1sTPVDMSQUgpdcaP65+nDw="));

//return;

var offers = new SteamTradeOffers();

var steamSession = null;

steamRelogin(function(){
	chekTrades();

	setInterval(function(){//Каждые 30 секунд проверяем трейды
		chekTrades();
	}, 30000);

	giveItems();

	setInterval(function(){//Каждые 30 секунд проверяем трейды
		giveItems();
	}, 30000);

	setInterval(function(){
		checkConfirmations();
	}, 10000);
});

function checkConfirmations(){//Одобрение всех подтверждений, если они есть
	steamSession.loggedIn(function(err, logged){//Проверяем авторизацию бота
		if(err){
			addToLog(err);
		}

		if(!logged){//Если не залогинен, то проходим еще раз авторизацию
			steamRelogin(function(){
				
			});

			return;
		}
	});

	var time = Math.floor(Date.now() / 1000);
    var confKey = SteamTotp.getConfirmationKey(steamAccountSettings.identitySecret, time, 'conf');
    var allowKey = SteamTotp.getConfirmationKey(steamAccountSettings.identitySecret, time, 'allow');

	steamSession.acceptAllConfirmations(time, confKey, allowKey, function(err, res){
		if(err){
			addToLog(err);
			return;
		}

		res.forEach(function(item){
			addToLog("Оффер " + item.id + " успешно подтвержден");
		});
	});
}

function giveItems(){//Функция для проверки предметов на выдачу
	steamSession.loggedIn(function(err, logged){//Проверяем авторизацию бота
		if(err){
			addToLog(err);
		}

		if(!logged){//Если не залогинен, то проходим еще раз авторизацию и снова чекаем трейды
			steamRelogin(function(){
				
			});

			return;
		}
	});
	addToLog("Проверка запросов на выдачу предметов");
	
	db.query("SELECT * FROM `user_items` WHERE `received`=0 AND `inwait`=1", function(err, result){//Берем из БД все невыданные предметы для пользователей
		//db.end();
		if(err){
			addToLog(err);
			return;
		}

		var itemsToReturn = {};

		result.forEach(function(item, i, arr){
			if(itemsToReturn[item.user_id] !== undefined){
				if(itemsToReturn[item.user_id][item.item_id] !== undefined){
					itemsToReturn[item.user_id][item.item_id].count += 1;
				}else{
					itemsToReturn[item.user_id][item.item_id] = {count: 1, user_id: item.user_id, item_id: item.item_id};
				}
			}else{
				itemsToReturn[item.user_id] = {};
				itemsToReturn[item.user_id][item.item_id] = {count: 1, user_id: item.user_id, item_id: item.item_id};
			}
			
			
			if(arr.length - 1 == i){
				sendOfferToWinner(itemsToReturn);
			}
		});
	});
}

function sendOfferToWinner(winners){//Проверка предметов для выдачи и запуск функции созщдания офферов
	addToLog("Проверка предметов для выдачи");

	offers.loadMyInventory({appId: 570, contextId: 2}, function(err, items){//Загружаем список наших предметов
		if(err){
			addToLog(err);
			return;
		}

		var botItems = {};//Список вещей, которые есть у бота и доступны для трейда

		var tradeOffers = {};//Список готовых трейд офферов

		items.forEach(function(item, i, array){
			botItems[item.classid] = {
				"amount": item.amount, 
				"trade": {"appid": item.appid, "contextid": item.contextid, "amount": 1, "assetid": item.id, "item_id" : 0}
			};


			if(array.length == i + 1){//Когда все предметы бота записаны
				
				for(var user_id in winners){
					db.query("SELECT * FROM `user` WHERE `id`=" + user_id, function(err, user){
						if(err){
							addToLog(err);
							return;
						}

						var gsteamid = user[0].steam_id;

						var tradeToken = user[0].trade_link.split("token=");
						tradeToken = tradeToken[1];

						for(var item_id in winners[user[0].id]){
							db.query("SELECT * FROM `items` WHERE `id`=" + item_id, function(err, data){
								if(err){
									addToLog(err);
									return;
								}

								if(botItems[data[0].steamid] !== undefined){
									if(botItems[data[0].steamid].amount >= winners[user[0].id][data[0].id].count){//Смотрим сколько в наличии у бота и сколько нужно вещей
										botItems[data[0].steamid].amount -= winners[user[0].id][data[0].id].count;
										botItems[data[0].steamid].trade.amount = winners[user[0].id][data[0].id].count;
										botItems[data[0].steamid].trade.item_id = winners[user[0].id][data[0].id].item_id;

										if(tradeOffers[gsteamid] !== undefined){
											tradeOffers[gsteamid].itemsFromMe.push(botItems[data[0].steamid].trade);
										}else{
											tradeOffers[gsteamid] = {
												partnerSteamId: gsteamid,
												itemsFromMe: [botItems[data[0].steamid].trade],
												itemsFromThem : [],
												accessToken: tradeToken,
												user_id: winners[user[0].id][data[0].id].user_id,
											};
										}

									}else{
										addToLog("У бота не достаточно вещей " + data[0].steamid + " для выдачи");
									}
								}else{
									addToLog("Вещи " + data[0].steamid + " нет у бота, необходимо пополнить список вещей бота");
								}
							});
						}

					});
				}

				setTimeout(function(){
					makeTrades(tradeOffers);
				}, 20000);
			}
		});
	});

}

function makeTrades(trades){//Отправляем трейды пользователям
	for(var id in trades){
		
		offers.makeOffer(trades[id], function(err, res){
			if(err){
				addToLog(err);
				return;
			}

			for(var item in trades[id].itemsFromMe){
				db.query("UPDATE `user_items` SET `received`=1, `inwait`=0 WHERE `item_id`=" + trades[id].itemsFromMe[item].item_id + " AND `user_id`=" + trades[id].user_id);
			}


			addToLog("Трейд " + res.tradeofferid + " успешно отправлен пользователю и ожидает подтверждения");
		});
	}
}

function chekTrades(){//Чекаем на новые трейды
	steamSession.loggedIn(function(err, logged){//Проверяем авторизацию бота
		if(err){
			addToLog(err);
		}

		if(!logged){//Если не залогинен, то проходим еще раз авторизацию и снова чекаем трейды
			steamRelogin(function(){
				
			});

			return;
		}
	});

	addToLog("Проверка новых трейдов");

	offers.getOffers({get_received_offers: 1, active_only: 1}, function(err, body){//Получаем список офферов
		if(err){
			addToLog(err);
		}
		
		if(!body || !body.response || !body.response.trade_offers_received){
			addToLog("Список трейдов пуст");
			return 0;
		}

		body.response.trade_offers_received.forEach(function (offer){
			if (offer.trade_offer_state !== 2) {
	        	return;
	        }

	        var isAdminOffer = false;

	        if(adminsID && adminsID.indexOf(offer.steamid_other) > -1){
	        	isAdminOffer = true;
	        	addToLog("Пришел обмен от админа (" + offer.steamid_other + ")");

	        	offers.acceptOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
					addToLog("Обмен от админа (" + offer.steamid_other + ") принят");
				});
	        }else{//Ищем в БД юзера, который прислал обмен
	        	addToLog("Пришел обмен от пользователя, ищем его в БД");

	        	if(offer.escrow_end_date != 0){
	        		addToLog("Пришел обмен с задержкой");
	        		offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
						addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
					});
					return;
	        	}

				db.query("SELECT * FROM `user` WHERE `steam_id`='" + offer.steamid_other + "'", function(err, result){

					if(result.length == 0){
						addToLog("Пользователя, приславший оффер, не найден в БД");
						offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
							addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
						});
						return;
					}else{
						if (offer.items_to_give && offer.items_to_give.length > 0){//Значит в обмене еще пытаются взять вещи с бота - отклоняем
				        	addToLog("Попытка забрать вещи у бота");
				        	offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
								addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
							});
				        }else{
				        	var summOfItems = 0;
				        	offer.items_to_receive.forEach(function(item, index, array){//Проходимся по списку вещей из трейда
				        		if(item.appid == 570){//Если вещь из доты
				        			
				        			db.query("SELECT * FROM `steam_items` WHERE `classid`='" + item.classid + "'", function(err, resultn){
										

										if(!resultn.length){//Если по ID не нашли в БД вещь
											addToLog("Вещь не найдена в БД");
											offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
												addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
											});
										}else{
											if(resultn[0].market_price > 5000){
												addToLog("Цена вещи больше 5 т.р.");
									        	offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
													addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
												});
											}else{
												summOfItems += resultn[0].market_price/2;//50% от стоимости на маркете

												if(index == array.length - 1){//Если последняя итерация
													offers.acceptOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, 
													function(error, result){
														if(error){
															addToLog(error);
															return;
														}

														db.query("UPDATE `user` SET `balance`=`balance`+" + summOfItems + " WHERE `steam_id`='" + offer.steamid_other + "'", function(err){
															if(err){
																addToLog("Ошибка во время обновления баланса пользователя");
																return;
															}

															addToLog("Баланс пользователя успешно обновлен");
														});
														
														addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " успешно принят");

													});
												}
											}
										}

									});
				        		}else{
				        			addToLog("Вещь из другой игры");
						        	offers.declineOffer({tradeOfferId: offer.tradeofferid, partnerSteamId: offer.steamid_other}, function(){
										addToLog("Обмен " + offer.tradeofferid + " от " + offer.steamid_other + " отклонен");
									});
				        		}
				        	});
				        }
					}
				});
	        }
	        
		});
	});

}

function steamRelogin(callback){//Логиним бота
	steamLogin(db_config, steamAccountSettings, steamLoginData, function(steam){
		steamSession = steam;
		offers.setup({
		  sessionID: steamLoginData.sessionID,
		  webCookie: steamLoginData.cookies,
		  APIKey: steamLoginData.apiKey
		});

		addToLog("Bot logged");

		if(callback){
			callback();
		}
	});
}

function steamLogin(db_config, settings, loginData, callback){//Логинимся и обновляем данные аккаунта и данные сессии 
	db.query("SELECT * FROM `settings` WHERE `name` IN ('bot_steam_login', 'bot_shared_secret', 'bot_identity_secret', 'bot_steam_password', 'steam_api_key')", function(err, result){
		//Берем ИЗ БД все данные
		if(err) addToLog(err);

		for(var i = 0; i < result.length; i ++){
			switch(result[i].name){
				case "bot_steam_login":
				settings.accountName = result[i].value;
				break;
				case "bot_shared_secret":
				settings.sharedSecret = result[i].value;
				break;
				case "bot_steam_password":
				settings.password = result[i].value;
				break;
				case "bot_identity_secret":
				settings.identitySecret = result[i].value;
				break;
				case "steam_api_key":
				loginData.apiKey = result[i].value;
				break;
			}
		}

		var steam = new SteamCommunity();

		var code = SteamTotp.generateAuthCode(settings.sharedSecret);

		steam.login({
			"accountName": settings.accountName,
			"password": settings.password,
			"twoFactorCode": code
		}, function(err, sessID, cookies, steamguard, oAuthToken){
			if(err) addToLog(err);

			loginData.sessionID = sessID;//Записываем данные сессии в объект
			loginData.cookies = cookies;
			loginData.steamguard = steamguard;
			loginData.oAuthToken = oAuthToken;

			if(callback){
				callback(steam);//Вызываем каллбек
			}

			steam.on("sessionExpired", function(err){//Если истекла сессия в стиме
				steamRelogin();
			});

			return steam;
		});

	});
}

function getAdminsID(){//Получение Steam ID админов
	addToLog("Ищем в БД ID админов");

	db.query("SELECT * FROM `settings` WHERE `name`='admin_steam_ids'", function(err, result){

		if(err) addToLog(err);

		if(result.length > 0){
			adminsID = result[0].value.split(",");
		}
	});
}

function addToLog(message){
	console.log(message);
}
