var winston    = require('winston');
var SteamUser = require('steam-user');
var SteamTotp = require('steam-totp');
var SteamCommunity = require('steamcommunity');
var TradeOfferManager = require('steam-tradeoffer-manager');
var mysql = require('mysql');
var config = require('./config.json');

var db_config = {//Данные для подключения к БД
	host: config.mysql_host,
	user: config.mysql_user,
	password: config.mysql_password,
	database: config.mysql_db
};

var logOnOptions = {
	"accountName": "",
	"password": "",
	"sharedSecret": "",
	"identitySecret": "",
	"twoFactorCode": ""
}

var client = new SteamUser();

var community = new SteamCommunity();

var manager = new TradeOfferManager({
	steam: client,
	community: community,
	language: 'en'
});

winston.configure({
	transports: [
		new (winston.transports.Console)(),
		new (winston.transports.File)({ filename: 'bot.log' })
	]
});

var db = mysql.createConnection(db_config);

db.query("SELECT * FROM setting", (err, rows) => {
	if(err){
		throw err;
	}

	rows.forEach((row, index, array) => {
		switch(row.name){
			case "bot_steam_login":
				logOnOptions.accountName = row.value;
			break;
			case "bot_steam_password":
				logOnOptions.password = row.value;
			break;
			case "bot_shared_secret":
				logOnOptions.sharedSecret = row.value;
			break;
			case "bot_identity_secret":
				logOnOptions.identitySecret = row.value;
			break;
		}

		if(index == array.length - 1){
			logOnOptions.twoFactorCode = SteamTotp.generateAuthCode(logOnOptions.sharedSecret);
			client.logOn(logOnOptions);
		}
	});
});

client.on('error', (err) => {
	winston.log('error', err);
});

client.on('loggedOn', () => {
	winston.info('Bot successfully logged');
});

client.on('webSession', (sessionid, cookies) => {
	winston.info('Cookies to manager and community successfully setted');
	manager.setCookies(cookies);

	community.setCookies(cookies);
	community.startConfirmationChecker(10000, logOnOptions.identitySecret);
	

	setInterval(() => {//Получение призов
		db.query("SELECT * FROM prize WHERE wait=1 AND sended=0  AND is_fake=0", (err, rows) => {
			if(!rows.length){
				return;
			}

			var items = {};

			rows.forEach((item, index, array) => {
				if(!items[item.user_id]){
					items[item.user_id] = {};
				}

				items[item.user_id][item.id] = item.item_id;

				if(index == array.length - 1){// Последняя итерация
					Object.keys(items).forEach((key) => {
						prepareOffer({"items": items[key], "user_id": key}, makeOfferFromData);
					});
				}
			});
		});
	}, 20000);
});

manager.on('newOffer', (offer) => {//Принимаем новые офферы

	if(offer.state != 2){
		return;
	}

	db.query("SELECT * FROM user WHERE username='" + offer.partner.getSteamID64() + "'", (errors, res, fields) => {
		if(!res.length){//Если юзера нет в БД
			winston.info('User with steamID ' + offer.partner.getSteamID64() + ' not found in our system');

			steamIdIsAdmin(offer.partner.getSteamID64(), (is_admin) => {//Проверяем может это админ
				if(is_admin){
					winston.info('Trade offer from admin.');

					offer.accept((err, status) => {
						if (err) {
							winston.log('error', err);
						} else {
							winston.info(`Accepted offer from admin. Status: ${status}.`);
						}
					});
				}
			});
			return;
		}else{//Если юзер есть в базе
			steamIdIsAdmin(offer.partner.getSteamID64(), (is_admin) => {
				if(is_admin){
					winston.info('Trade offer from admin.');

					offer.accept((err, status) => {
						if (err) {
							winston.log('error', err);
						} else {
							winston.info(`Try to accept offer from admin. Status: ${status}.`);
						}
					});
				}else{
					winston.info("Trade offer from user.");

					if(offer.itemsToGive.length > 0 || offer.escrowEnds){//У нас хотят забрать вещи или стоит escrow
						offer.decline((err) => {
							if (err) {
								winston.debug('error', err);
							} else {
								winston.info('User tried to take our items or user has escrow. Offer declined.');
							}
						});

						return;
					}

					var summOfItems = 0;

					offer.itemsToReceive.forEach((item, index, array) => {//Перебираем все пришедшие вещи
						if(item.appid != 730){//Если вещь не из кс го
							offer.decline((err) => {
								if (err) {
									winston.debug('error', err);
								} else {
									winston.info('User tried to give item from another game. Offer declined.');
								}
							});

							return;
						}

						db.query("SELECT * FROM steam_item WHERE hash_name='" + item.market_hash_name + "'", (err, db_item) => {
							if(!db_item.length){//Вещь не была найдена в нашей БД
								offer.decline((err) => {
									if (err) {
										winston.debug('error', err);
									} else {
										winston.info('Item "' + item.market_name + '" not found in our database. Offer declined.');
									}
								});

								return;
							}

							summOfItems += db_item[0].price / 2;//50% от стоимости вещи

							if(index == array.length - 1){//Последняя итерация, нужно принимать обмен
								offer.accept((err, status) => {
									if (err) {
										winston.log('error', err);
									} else {
										winston.info(`Offer from ${offer.partner.getSteamID64()} accepted.`);

										db.query("UPDATE user SET balance=balance+" + summOfItems + " WHERE username='" + offer.partner.getSteamID64() + "'", (err) => {
											if(err){
												winston.debug('error', err);
											}else{
												winston.info(`Balance of user ${offer.partner.getSteamID64()} has been updated.`);
											}
										});
									}
								});
							}

						});
					});
				}
			});
		}
	});
});

function makeOfferFromData(data){
	manager.loadInventory(730, 2, true, (err, inventory) => {
		if(err){
			winston.log('error', err);
			return;
		}

		db.query("SELECT * FROM user WHERE id=" + data.user_id, (uerr, urows) => {
			if(uerr){
				winston.log('error', uerr);
				return;
			}

			if(!urows.length){
				return;
			}

			if(!urows[0].trade_link){
				return;
			}

			var link = urows[0].trade_link.replace('&amp;', '&');

			var offer = manager.createOffer(link);
			var sended = [];
			var notSended = [];

			inventory.forEach((item, index, array) => {
				var added = false;
				Object.keys(data.items).forEach((key, item_index, item_array) => {
					if(item.market_hash_name == data.items[key] && !added){//значит есть у бота
						offer.addMyItem(item);
						sended[sended.length] = key;

						data.items[key] = null;
						added = true;
					}else if(!added && data.items[key] != null && index == array.length - 1){
						notSended[notSended.length] = data.items[key];
					}
				});

				if(index == array.length - 1){//Пора отправлять оффер
					if(sended.length){//Есть что отправить
						offer.send((err, status) => {
							if (err) {
								winston.log('error', err);
							} else {
								winston.info(`Sent offer to ${urows[0].username} (${link}). Status: ${status}.`);
								
								sended = sended.join(',');

								db.query("UPDATE prize SET sended=1 WHERE id IN(" + sended + ")");
							}
						});
					}

					var logItems = {};

					notSended.forEach((name, i, arr) => {
						if(logItems[name]){
							logItems[name] ++;
						}else{
							logItems[name] = 1;
						}

						if(i == arr.length - 1){
							Object.keys(logItems).forEach((name) => {
								winston.info('Bot dont has "' + name + '" X ' + logItems[name]);
							});
						}
					});
				}
			});
		});

	});
}

function prepareOffer(data, callback){
	Object.keys(data.items).forEach((key, index, array) => {
		db.query("SELECT * FROM case_item WHERE id=" + data.items[key], (err, rows) => {
			if(err){
				winston.log('error', err);
			}

			db.query("SELECT * FROM steam_item WHERE id=" + rows[0].item, (err2, rows2) => {
				data.items[key] = rows2[0].hash_name;
		
				if(index == array.length - 1){
					callback(data);
				}
			});
			
		});
	});
}

function steamIdIsAdmin(steamID, callback){//Проверяем является ли данный ID админом
	db.query("SELECT * FROM setting WHERE name='steam_admin_ids'", (errs, res, flds) => {
		var is_admin = false;

		if(!res.length || !res[0].value){
			winston.info("Admin IDS on database is empty");
		}else{
			admins = res[0].value.split(",");

			for(var i = 0; i < admins.length; i ++){
				if(admins[i] == steamID){
					is_admin = true;
				}
			}
		}

		callback(is_admin);
	});
}

setInterval(function(){
	db.query("SELECT 1");
}, 5000);