<?

namespace fronted\app;

class Thread{
	private $url = null;

	public function __construct($url){
		$this->url = $url;
	}

	public function start(){
		if(empty($this->url)){
			return false;
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		//curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}
}