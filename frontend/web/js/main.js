var socket = io.connect(':1448');

$(document).ready(function(){
	$(document).click(function(event){
		if($('.ghead .right .item.language').has(event.target).length === 0){
			$('.ghead .right .item.language').removeClass("active");
		}
	});

	$('.main_block .content .primary .gcase .head').click(function(event){
		$(this).toggleClass('hidden');

		if($(this).hasClass("hidden")){
			$(this).find('.hide span').text(language.show_case);
		}else{
			$(this).find('.hide span').text(language.hide_case);
		}
	});

	$('.main_block .content .primary .open_case .info .open .count .dropdown li').click(function(event){
		$(this).parent().parent().find(".value").text($(this).text());
	});

	$('.tooltip').tooltipster({
		theme: 'tooltipster-borderless',
		arrow: false,
		animationDuration: 1,
		//animation: 'slide',
		delay: 1
	});

	socket.on("online_stat", function(data){
		var online = data.online;

		online = online.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

		$('.main_block .content .stats .item#online-stat .value').text(online);
	});

	socket.on("new_prize", function(data){
		var item = '<div class="item" style="box-shadow: 3px 0 0 #' + data.color + ' inset;-webkit-box-shadow: 3px 0 0 #' + data.color + ' inset;">\
            <img class="avatar" src="' + data.photo + '" alt="">\
            <span class="title">' + data.title + '</span>\
            <div class="data">\
                <div class="avatar" style="background-image:url(\'' + data.user_photo + '\');"></div>\
                <span class="title">' + data.title + '</span>\
            </div>\
        </div>';

        $('.main_block .sidebar > .logotype').after(item);

        var count = parseInt($('.main_block .content .stats .item#opened-stat .value').text().replace(/[^0-9.]/g, '')) + 1;
        $('.main_block .content .stats .item#opened-stat .value').text(count.toLocaleString());
	});

	socket.on("quantity_changed", function(data){
		if(data.quantity){
			$('.main_block .content .stats .item#quantity-stat .value').text(data.quantity + "%");
		}
	});

	socket.on("unique_players_changed", function(){
		var count = parseInt($('.main_block .content .stats .item#users-stat .value').text().replace(/[^0-9.]/g, '')) + 1;
        $('.main_block .content .stats .item#users-stat .value').text(count.toLocaleString());
	});
});

function sendFeedback(elem){
	if(!$('.feedback_form textarea').length){
		notify(language.need_auth);
		return;
	}

	if($('.feedback_form').css('display') == "none"){
		$('.feedback_form').css('display', 'block');
		$(document).scrollTop($('.feedback_form').offset().top);
		return;
	}

	var data = {feedback: $(".feedback_form textarea").val()};

	var CSRF = getCSRF();

	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: "/index.php?r=site/send-feedback",
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			$(".feedback_form textarea").val("");
			notify(language.feedback_sended, "success");
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function generatePayment(elem){
	var summ = $(elem).parent().find("input").val();

	var data = {summ: summ};
	var CSRF = getCSRF();
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: "/index.php?r=site/generate-payment",
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			$("#payment").remove();

			$("body").append(data.form);
			$("#payment").submit();
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function saveTradeLink(elem){
	var link = $(elem).parent().find("input").val();
	var data = {link: link};
	var CSRF = getCSRF();
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: "/index.php?r=site/save-trade-link",
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			notify(language.link_saved, "success");
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function changeLaunge(elem){
	$(elem).toggleClass("active");
}

function RollCase(id, elem, skip){
	if($(elem).hasClass("progress")){
		return false;
	}

	$(elem).addClass("progress");

	if($(elem).hasClass('opened') && !skip){
		openCasePopup(id, function(){
			$(elem).removeClass("progress");
			RollCase(id, elem, true);
		});

		return;
	}

	var data = {case: id};
	var CSRF = getCSRF();
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: "/index.php?r=site/roll-case",
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			$('.ghead .right .steam .balance').text(data.balance + "р.");

			$('.case_popup .list_of_items .items .item:nth-child(496)').html('<div class="item">\
				<div class="name">' + data.prize.title + '</div>\
				<div class="image">\
					<img src="' + data.prize.image + '" alt="">\
				</div>\
			</div>');

			$('.case_popup .list_of_items .items').addClass('roll');
			
			setTimeout(function(){
				$(elem).removeClass("progress").addClass("opened");

				$('.case_popup > .win_title').css('display', 'block');

				$('.case_popup .win_items').prepend('<div class="item">\
					<div class="image" style="border-color: #' + data.prize.color + ';">\
						<img src="' + data.prize.image + '" alt="">\
					</div>\
					<div class="title">' + data.prize.title + '</div>\
				</div>');

				socket.emit("open_case", {id: data.id, lang: $('.ghead .right .item.language span').text()});
			}, 5000);
			
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function RollDayliCase(elem){
	if($(elem).hasClass("progress")){
		return false;
	}

	$(elem).addClass("progress");

	var data = {};
	var CSRF = getCSRF();
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: "/index.php?r=site/roll-dayli-case",
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			$('.case_popup .list_of_items .items .item:nth-child(496)').html('<div class="item">\
				<div class="name">' + data.prize.title + '</div>\
				<div class="image">\
					<img src="' + data.prize.image + '" alt="">\
				</div>\
			</div>');

			$('.case_popup .list_of_items .items').addClass('roll');
			
			setTimeout(function(){
				$(elem).removeClass("progress").addClass("opened");

				$('.case_popup > .win_title').css('display', 'block');

				$('.case_popup .win_items').prepend('<div class="item">\
					<div class="image" style="border-color: #' + data.prize.color + ';">\
						<img src="' + data.prize.image + '" alt="">\
					</div>\
					<div class="title">' + data.prize.title + '</div>\
				</div>');
				$('.ghead .right .steam .balance').text(data.balance + "р.");
			}, 5000);
			
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function openCasePopup(id, callback){
	var CSRF = getCSRF();
	var data = {case: id};
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/index.php?r=site/load-case",
		data: data,
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			var items = [];

			for(var i = 0; i < data.items.length; i ++){
				items[items.length] = '<div class="item">\
					<div class="name">' + data.items[i].title + '</div>\
					<div class="image">\
						<img src="' + data.items[i].image + '" alt="">\
					</div>\
				</div>';
			}

			$('.case_popup .list_of_items .items').html('').removeClass('roll').html(items);

			if(!callback){
				$('.case_popup .win_items .item:not(.empty)').remove();
				$('.case_popup > .win_title').css('display', 'none');
				$('.case_popup').fadeIn('fast');
				$('.case_popup  .open_case_btn').removeClass('opened');
			}else{
				callback();
			}
			
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function openCaseDayli(elem){
	var CSRF = getCSRF();
	var data = {};
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/index.php?r=site/load-dayli",
		data: data,
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			var items = [];

			for(var i = 0; i < data.items.length; i ++){
				items[items.length] = '<div class="item">\
					<div class="name">' + data.items[i].title + '</div>\
					<div class="image">\
						<img src="' + data.items[i].image + '" alt="">\
					</div>\
				</div>';
			}

			$('.case_popup .list_of_items .items').html('').removeClass('roll').html(items);

			
			$('.case_popup .win_items .item:not(.empty)').remove();
			$('.case_popup > .win_title').css('display', 'none');
			$('.case_popup').fadeIn('fast');
			$('.case_popup  .open_case_btn').removeClass('opened');
			
			
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function sendMePrize(){
	var CSRF = getCSRF();
	var data = {};
	data[CSRF.param] = CSRF.content;

	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/index.php?r=site/send-prize",
		data: data,
		success: function(data){
			if(!data.success){
				notify(data.message);
				return;
			}

			notify(language.prize_sended, "success");
		},
		error: function(){
			notify(language.ajax_error);
		}
	});
}

function getCSRF(){
	return {"param": yii.getCsrfParam(), "content": yii.getCsrfToken()};
}

function notify(message, type, timeout){

	if(!type){
		type = "error";
	}

	if(!timeout){
		timeout = 3500;
	}

	new Noty({
	    text: message,
	    type: type,
	    timeout: timeout
	}).show();
}