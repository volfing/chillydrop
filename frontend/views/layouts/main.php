<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Stat;
use common\models\Setting;
use common\models\User;
use common\models\Prize;
use common\models\CaseItem;
use common\models\SteamItem;


$online = Stat::findOne(["type" => "online"]);
$online = $online->value + (int)Setting::findByKey('online_plus')->value + (int)Stat::findOne(['type' => 'fake_online'])->value;

$unique_players = count(User::find()->where(['is_real' => 1])->all()) + (int)Setting::findByKey('uniq_users_plus')->value + (int)Stat::findOne(['type' => 'unique_players'])->value;

$drop_qunatity = (int)Setting::findByKey('drop_quantity')->value;

//$opened_cases = count(Prize::find()->all()) + (int)Setting::findByKey('opened_plus')->value;
$opened_cases = Stat::findOne(['type' => 'case_opened'])->value + (int)Setting::findByKey('opened_plus')->value;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="http://<?=$_SERVER["SERVER_NAME"]?>:1448/socket.io/socket.io.js"></script>
    <script>
        var language = <?=json_encode($this->params['language_dictionary']);?>;
    </script>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="container">
        <header class="ghead">
            <div class="left">
                <a href="/"><img style="opacity: 0;" src="/images/logotype.png" alt=""></a>
                <div class="social">
                    <?
                    $twitter = Setting::findByKey('twitter_link')->value;
                    $facebook = Setting::findByKey('fb_link')->value;
                    $vk = Setting::findByKey('vk_link')->value;
                    ?>
                    <?if(!empty($twitter)):?>
                        <a href="<?=$twitter?>" target="_blank"><img src="/images/twitter.png" alt=""></a>
                    <?endif;?>

                    <?if(!empty($facebook)):?>
                        <a href="<?=$facebook?>" target="_blank"><img src="/images/facebook.png" alt=""></a>
                    <?endif;?>

                    <?if(!empty($vk)):?>
                        <a href="<?=$vk?>" target="_blank"><img src="/images/vk.png" alt=""></a>
                    <?endif;?>
                </div>
            </div>

            <div class="right">
                <div class="item language" onclick="changeLaunge(this);">
                    <img src="/images/globus.png" alt="">
                    <?if($this->params['language_prefix'] != 'en'):?>
                        <span>RU</span>
                    <?else:?>
                        <span>ENG</span>
                    <?endif;?>
                    <img src="/images/arrow_down.png" alt="">
                    <ul class="dropdown">
                        <li><a href="/?language=ru">RU</a></li>
                        <li><a href="/?language=en">ENG</a></li>
                    </ul>
                </div>
                <a href="/index.php?r=site/feedbacks" class="item tooltip" title="<?=$this->params['language_dictionary']['top_nav_1']?>"><img src="/images/feedbacks.png" alt=""></a>
                <a href="/index.php?r=site/page&id=3" class="item tooltip" title="<?=$this->params['language_dictionary']['top_nav_2']?>"><img src="/images/bonus.png" alt=""></a>
                <a href="/index.php?r=site/dayli" class="item tooltip" title="<?=$this->params['language_dictionary']['top_nav_3']?>"><img src="/images/box.png" alt=""></a>
                <a href="/index.php?r=site/faq" class="item tooltip" title="<?=$this->params['language_dictionary']['top_nav_4']?>"><img src="/images/faq.png" alt=""></a>
                <?if(Yii::$app->user->isGuest):?>
                    <a href="/index.php?r=site/login&login=1" class="steam"><img src="/images/steam.png" alt=""><span><?=$this->params['language_dictionary']['sigin']?></span></a>
                <?else:?>
                    <a href="/index.php?r=site/profile" class="steam">
                        <div class="group">
                            <div style="background-image: url('<?=Yii::$app->user->identity->photo?>');border-radius:100%;width:30px;height:30px;background-size:cover;background-position: center;"></div>
                            <span><?=Yii::$app->user->identity->login?></span>
                        </div>
                        <div class="balance"><?=number_format(Yii::$app->user->identity->balance, 0, ".", " ");?>р.</div>
                    </a>
                    <a href="/index.php?r=site/logout" class="steam" style="align-self: stretch;padding: 5px 10px;"><span style="margin-left: 0;"><?=$this->params['language_dictionary']['logout']?></span></a>
                <?endif;?>
            </div>
        </header>
    </div>

    <div class="container main_block">
        <!--SIDEBAR START!-->
        <div class="sidebar">
            <a href="/" class="logotype"><img src="/images/logotype.png" alt=""></a>

            <?
            $lastprizes = Prize::find()->orderBy(['id' => SORT_DESC])->limit(30)->all();
            ?>
            
            <?foreach($lastprizes as $prize):?>
                <?
                $user_data = User::findOne($prize->user_id);
                $item_data = CaseItem::findOne($prize->item_id);
                $steam_item_data = SteamItem::findOne($item_data->item);

                if($this->params['language_prefix'] != "ru"){
                    $item_data->title = $item_data->eng_title;
                }

                $title = !empty($item_data->title) ? $item_data->title : $steam_item_data->name;
                $image = !empty($item_data->image) ? $item_data->image : $steam_item_data->image;
                ?>
                <div class="item" style="box-shadow: 3px 0 0 #<?=$steam_item_data->color?> inset;-webkit-box-shadow: 3px 0 0 #<?=$steam_item_data->color?> inset;">
                    <img class="avatar" src="<?=$image?>" alt="">
                    <span class="title"><?=$title?></span>
                    <div class="data">
                        <div class="avatar" style="background-image:url('<?=$user_data->photo?>');"></div>
                        <span class="title"><?=$title?></span>
                    </div>
                </div>
            <?endforeach;?>
        </div>
        <!--SEDEBAR END!-->
        <div class="content">
            <!--STATS START!-->
            <div class="stats">
                <div class="item" id="opened-stat">
                    <img src="images/stat-opened.png" alt="">
                    <div class="data">
                        <div class="title"><?=$this->params['language_dictionary']['stat_opened']?></div>
                        <div class="value"><?=number_format($opened_cases, 0, ".", " ")?></div>
                    </div>
                </div>
                <div class="item" id="quantity-stat">
                    <img src="images/stat-quantity.png" alt="">
                    <div class="data">
                        <div class="title"><?=$this->params['language_dictionary']['stat_quantity']?></div>
                        <div class="value"><?=$drop_qunatity?>%</div>
                    </div>
                </div>
                <div class="item" id="users-stat">
                    <img src="images/stat-users.png" alt="">
                    <div class="data">
                        <div class="title"><?=$this->params['language_dictionary']['stat_uniq']?></div>
                        <div class="value"><?=number_format($unique_players, 0, ".", " ")?></div>
                    </div>
                </div>
                <div class="item" id="online-stat">
                    <img src="images/stat-online.png" alt="">
                    <div class="data">
                        <div class="title"><?=$this->params['language_dictionary']['stat_online']?></div>
                        <div class="value"><?=number_format($online, 0, ".", " ")?></div>
                    </div>
                </div>
            </div>
            <!--STATS END!-->

            <div class="primary">
                <?= Alert::widget() ?>
                <?= $content ?>
                <footer class="gfooter">
                    <div class="fline"><a href="/"><img src="/images/logotype.png" alt=""></a> <div class="copyr"><?=$_SERVER['HTTP_HOST']?> © 2017</div></div>
                    <div class="sline cards">
                        <img src="/images/payment-logo-online-mastercard-method-finance-icon.png" alt="">
                        <img src="/images/89115_card_512x512.png" alt="">
                    </div>
                </footer>
            </div>

        </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
