<?
$this->title = $this->params['language_dictionary']['top_nav_1'];
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt=""><?=$this->params['language_dictionary']['home_title']?></a> <span class="separator">></span> <span><?=$this->params['language_dictionary']['top_nav_1']?></span></div>

<div class="feedbacks_block">
	<div onclick="sendFeedback(this);" class="submit"><?=$this->params['language_dictionary']['send_feedback']?></div>

	<div class="items">
		<?foreach($feedbacks as $feedback):?>
			<div class="item">
				<div class="avatar">
					<img src="/images/avatar_bg.png" alt="">
					<div class="photo" style="background-image: url('<?=$feedback['user_photo']?>');">
						<img src="/images/avatar_front_bg.png" alt="" class="bg">
					</div>
				</div>
				<div class="data">
					<div class="name"><?=$feedback['user_name']?></div>
					<div class="text">
						<?=$feedback['text']?>
					</div>
				</div>
			</div>
		<?endforeach;?>
		
		<?if(!Yii::$app->user->isGuest):?>
			<div class="feedback_form">
				<textarea name="feedback" placeholder="<?=$this->params['language_dictionary']['feedback_text']?>"></textarea>
			</div>
		<?endif;?>

		<div onclick="sendFeedback(this);" class="submit" style="margin-bottom: 40px;"><?=$this->params['language_dictionary']['send_feedback']?></div>

	</div>
</div>