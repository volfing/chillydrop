<?
use common\models\SteamItem;

$this->title = $title;
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt=""><?=$this->params['language_dictionary']['home_title']?></a> <span class="separator">></span> <span><?=$title?></span></div>
				
<!--OPEN START!-->
<div class="open_case">
	<img src="/images/opencase_bg.png" alt="">
	<div class="case">
		<div class="image">
			<img src="<?=$image1?>" alt="">
			<img class="front" src="<?=$image2?>" alt="">
		</div>

		<div class="info">
			<!-- <span><?=$this->params['language_dictionary']['cases_for_opening']?>:</span> -->
			<div class="open">
				<div class="count" style="display: none;">
					<span class="value">1</span>
					<img src="images/case_count.png" alt="">
					<ul class="dropdown">
						<li>1</li>
						<li>2</li>
						<li>3</li>
						<li>4</li>
						<li>5</li>
						<li>6</li>
						<li>7</li>
						<li>8</li>
						<li>9</li>
						<li>10</li>
					</ul>
				</div>
				<div style="margin-left: 0;" class="button" onclick="openCasePopup(<?=$id?>);"><?=$this->params['language_dictionary']['open_case']?></div>
			</div>
			<span><?=$this->params['language_dictionary']['price_of_opening']?> <?=number_format($price, 0, '.', ' ')?> р.</span>
		</div>
	</div>
</div>
<!--OPEN END!-->

<!--LIST OF CASE START!-->
<div class="gptitle"><?=$this->params['language_dictionary']['case_items']?></div>
<div class="items_in_case">
	<?foreach($items as $item):?>
		<?
		$steamItem = SteamItem::findOne($item["item"]);

		if($this->params['language_prefix'] != "ru"){
			$item['title'] = $item['eng_title'];
		}
		?>
		<div class="item" style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem['color']?> inset;box-shadow: 3px 0 0 #<?=$steamItem['color']?> inset;">
			<div class="image">
				<?if(!empty($item['image'])):?>
					<img src="<?=$item['image']?>" alt="">
				<?else:?>
					<img src="<?=$steamItem['image']?>" alt="">
				<?endif;?>
			</div>
			<div class="data">
				<?if(!empty($item['title'])):?>
					<span class="title" title="<?=$item['title']?>"><?=str_replace(" | ", "<br>", $item['title'])?></span>
				<?else:?>
					<span class="title" title="<?=$steamItem['name']?>"><?=str_replace(" | ", "<br>", $steamItem['name'])?></span>
				<?endif;?>
			</div>
		</div>
	<?endforeach;?>

	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
	<div class="item empty"></div>
</div>
<!--LIST OF CASE END!-->

<!--POPUP OPEN START!-->
<div class="case_popup" style="display: none;">
	<img src="images/close_modal.png" alt="" class="close" onclick="$(this).parent().fadeOut('fast');">
	<div class="case_title">Открыть "<?=$title?>"</div>
	
	<div class="connector">
		<div class="arrows"></div>
		<div class="list_of_items">
			<div class="items">
				
			</div>
		</div>
	</div>

	<div class="open_case_btn" onclick="RollCase(<?=$id?>, this);"><?=$this->params['language_dictionary']['open_case']?></div>

	<div style="font-size: 13px;margin: -30px auto 50px auto;display: table;"><?=$this->params['language_dictionary']['price_of_opening']?> <?=number_format($price, 0, '.', ' ')?> р.</div>
	
	<div class="win_title"><?=$this->params['language_dictionary']['your_win']?> </div>

	<div class="win_items">
		
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
	</div>
</div>
<!--POPUP OPEN END!-->