<?
use common\models\SteamItem;
use common\models\CaseItem;

$this->title = $this->params['language_dictionary']['dayli_free'];
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt="">Главная</a> <span class="separator">></span> <span><?=$this->title?></span></div>

<div class="dayli_free">
	<img src="<?=$data->banner?>" alt="" class="banner">

	<div class="gptitle"><?=$this->params['language_dictionary']['you_can_get_free']?></div>
	<div class="items_in_case">
		
		<?
		if($this->params['language_prefix'] != 'ru'){
			$data->name_of_prize = $data->eng_name_of_prize;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #58f33f inset;box-shadow: 3px 0 0 #58f33f inset;" class="item">
			<div class="image">
				<img src="<?=$data->image_of_prize?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$data->name_of_prize?>"><?=str_replace(" | ", "<br>", $data->name_of_prize)?></span>
			</div>
		</div>
	
		<?
		$item = CaseItem::findOne($data->item1);

		if(!empty($item->eng_title) && $this->params['language_prefix'] != 'ru'){
			$item->title = $item->eng_title;
		}elseif(empty($item->title) || empty($item->eng_title)){
			$steamItem = SteamItem::findOne($item->item);

			$item->title = $steamItem->name;
		}

		if(empty($item->image)){
			$item->image = $steamItem->image;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;" class="item">
			<div class="image">
				<img src="<?=$item->image?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$item->title?>"><?=str_replace(" | ", "<br>", $item->title)?></span>
			</div>
		</div>

		<?
		$item = CaseItem::findOne($data->item2);

		if(!empty($item->eng_title) && $this->params['language_prefix'] != 'ru'){
			$item->title = $item->eng_title;
		}elseif(empty($item->title) || empty($item->eng_title)){
			$steamItem = SteamItem::findOne($item->item);

			$item->title = $steamItem->name;
		}

		if(empty($item->image)){
			$item->image = $steamItem->image;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;" class="item">
			<div class="image">
				<img src="<?=$item->image?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$item->title?>"><?=str_replace(" | ", "<br>", $item->title)?></span>
			</div>
		</div>

		<?
		$item = CaseItem::findOne($data->item3);

		if(!empty($item->eng_title) && $this->params['language_prefix'] != 'ru'){
			$item->title = $item->eng_title;
		}elseif(empty($item->title) || empty($item->eng_title)){
			$steamItem = SteamItem::findOne($item->item);

			$item->title = $steamItem->name;
		}

		if(empty($item->image)){
			$item->image = $steamItem->image;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;" class="item">
			<div class="image">
				<img src="<?=$item->image?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$item->title?>"><?=str_replace(" | ", "<br>", $item->title)?></span>
			</div>
		</div>

		<?
		$item = CaseItem::findOne($data->item4);

		if(!empty($item->eng_title) && $this->params['language_prefix'] != 'ru'){
			$item->title = $item->eng_title;
		}elseif(empty($item->title) || empty($item->eng_title)){
			$steamItem = SteamItem::findOne($item->item);

			$item->title = $steamItem->name;
		}

		if(empty($item->image)){
			$item->image = $steamItem->image;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;" class="item">
			<div class="image">
				<img src="<?=$item->image?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$item->title?>"><?=str_replace(" | ", "<br>", $item->title)?></span>
			</div>
		</div>

		<?
		$item = CaseItem::findOne($data->item5);

		if(!empty($item->eng_title) && $this->params['language_prefix'] != 'ru'){
			$item->title = $item->eng_title;
		}elseif(empty($item->title) || empty($item->eng_title)){
			$steamItem = SteamItem::findOne($item->item);

			$item->title = $steamItem->name;
		}

		if(empty($item->image)){
			$item->image = $steamItem->image;
		}
		?>

		<div style="-webkit-box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;box-shadow: 3px 0 0 #<?=$steamItem->color?> inset;" class="item">
			<div class="image">
				<img src="<?=$item->image?>" alt="">
			</div>
			<div class="data">
				<span class="title" title="<?=$item->title?>"><?=str_replace(" | ", "<br>", $item->title)?></span>
			</div>
		</div>

		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
	</div>

	<div class="submit" onclick="openCaseDayli(this);"><?=$this->params['language_dictionary']['open_in_modal_case']?></div>
</div>

<!--POPUP OPEN START!-->
<div class="case_popup" style="display: none;">
	<img src="/images/close_modal.png" alt="" class="close" onclick="$(this).parent().fadeOut('fast');">
	<div class="case_title">Открыть "<?=$this->title?>"</div>
	
	<div class="connector">
		<div class="arrows"></div>
		<div class="list_of_items">
			<div class="items">
				
			</div>
		</div>
	</div>

	<div class="open_case_btn" onclick="RollDayliCase(this);"><?=$this->params['language_dictionary']['open_case']?></div>
	
	<div class="win_title"><?=$this->params['language_dictionary']['your_win']?> </div>

	<div class="win_items">
		
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
	</div>
</div>
<!--POPUP OPEN END!-->