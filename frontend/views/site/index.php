<?php

/* @var $this yii\web\View */

$this->title = 'Chilly Drop';
?>

<?foreach($cases as $group):?>
    <div class="gcase" <?=(!empty($group['background']) ? 'style="background-image:url(\'' . $group['background'] . '\');"' : '')?>>
        <div class="head">
            <div class="title"><img src="<?=$group["icon"]?>" alt=""><?=$group['title']?></div>
            <div class="hide"><span><?=$this->params['language_dictionary']['hide_case']?></span> <img src="/images/case_arrow.png" alt=""></div>
        </div>
        <div class="list">
            <?foreach($group["cases"] as $case):?>
                <a href="/index.php?r=site/case&id=<?=$case['id']?>" class="case">
                    <div class="image">
                        <img src="<?=$case['image1']?>" alt="" class="bg">
                        <img src="<?=$case['image2']?>"" alt="" class="front">
                    </div>
                    <div class="title"><?=$case['title']?></div>
                    <div class="price"><?=$case['price']?> <span>р.</span></div>
                </a>
            <?endforeach;?>

            <div class="case empty"></div>
            <div class="case empty"></div>
            <div class="case empty"></div>
            <div class="case empty"></div>
        </div>
    </div>
<?endforeach;?>