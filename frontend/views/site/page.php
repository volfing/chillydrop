<?
if($this->params['language_prefix'] != "ru"){
	$page->title = $page->eng_title;
	$page->content = $page->eng_content;
}
$this->title = $page->title;
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt="">Главная</a> <span class="separator">></span> <span><?=$page->title?></span></div>

<div>
	<?=$page->content?>
</div>