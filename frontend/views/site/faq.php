<?
$this->title = "FAQ";
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt=""><?=$this->params['language_dictionary']['home_title']?></a> <span class="separator">></span> <span><?=$this->title?></span></div>

<div class="faq_block">
	<?foreach($questions as $question):?>
		<?
		if($this->params['language_prefix'] != "ru"){
			$question->title = $question->title_eng;
			$question->answer = $question->answer_eng;
		}
		?>
		<div class="question">
			<div class="title"><i></i><?=$question->title?></div>
			<div class="answer"><?=$question->answer?></div>
		</div>
	<?endforeach;?>

	<div class="question empty"></div>
	<div class="question empty"></div>
	<div class="question empty"></div>
	<div class="question empty"></div>
</div>