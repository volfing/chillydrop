<?
$this->title = 'Профиль';
?>

<div class="breadcrumbs"><a href="/"><img src="/images/breadcrumbs.png" alt=""><?=$this->params['language_dictionary']['home_title']?></a> <span class="separator">></span> <span><?=$this->params['language_dictionary']['profile_title']?></span></div>
				
<!--PROFILE START!-->
<div class="top_profile">
	<div class="avatar">
		<img src="/images/avatar_bg.png" alt="">
		<div class="photo" style="background-image: url('<?=Yii::$app->user->identity->photo?>');">
			<img src="/images/avatar_front_bg.png" alt="" class="bg">
		</div>

		<div class="balance"><?=$this->params['language_dictionary']['balance']?> <?=number_format(Yii::$app->user->identity->balance, 0, ".", " ");?>р.</div>
	</div>

	<div class="information">
		<span><?=$this->params['language_dictionary']['profile_info_1']?></span>
		<?=$this->params['language_dictionary']['profile_info_2']?>
		<span></span>
		<?=$this->params['language_dictionary']['profile_info_3']?>
	</div>
</div>

<div class="profile_block">
	<div class="gradient_title"><?=$this->params['language_dictionary']['payment_title']?></div>
	<div class="description"><?=$this->params['language_dictionary']['payment_info']?></div>
	<div class="input">
		<input type="text" name="summ" value="100">
		<div class="submit" onclick="generatePayment(this);"><?=$this->params['language_dictionary']['payment_cash']?></div>
		<a href="<?=$trade_link?>" class="submit bg2" target="_blank"><?=$this->params['language_dictionary']['payment_skins']?></a>
	</div>
</div>
<div class="profile_block">
	<div class="gradient_title">TRADE URL</div>
	<div class="description"><a href="http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url" target="_blank"><?=$this->params['language_dictionary']['trade_link']?></a> — <?=$this->params['language_dictionary']['trade_link_info']?></div>
	<div class="input">
		<input style="width: 390px;" type="text" name="link" placeholder="<?=$this->params['language_dictionary']['your_trade_link']?>" <?=!empty(Yii::$app->user->identity->trade_link) ? 'value="' . Yii::$app->user->identity->trade_link . '"' : ''?>>
		<div class="submit bg3" onclick="saveTradeLink(this);"><?=$this->params['language_dictionary']['save']?></div>
	</div>
</div>
<!--PROFILE END!-->

<?if(!empty($items)):?>
	<!--LIST OF CASE START!-->
	<div class="gptitle"><?=$this->params['language_dictionary']['your_items']?></div>
	<div class="items_in_case">
		
		<?foreach($items as $item):?>

			<div style="-webkit-box-shadow: 3px 0 0 #<?=$item['color']?> inset;box-shadow: 3px 0 0 #<?=$item['color']?> inset;" class="item">
				<div class="image">
					<img src="<?=$item['image']?>" alt="">
				</div>
				<div class="data">
					<span class="title" title="<?=$item['title']?>"><?=str_replace(" | ", "<br>", $item['title'])?></span>
				</div>
			</div>

		<?endforeach;?>

		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
		<div class="item empty"></div>
	</div>
	<div class="profile_block" >
		<div class="input">
			<div class="submit bg3" style="margin-left: 0;" onclick="sendMePrize();"><?=$this->params['language_dictionary']['get_items']?></div>
		</div>
	</div>
<?endif;?>
<!--LIST OF CASE END!-->