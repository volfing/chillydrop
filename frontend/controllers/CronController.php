<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\Setting;
use common\models\SteamItem;

class CronController extends Controller
{
	private $key = "dowamod9021390123010okdwa0_Az-awd2A";

    public function beforeAction($action){
        $this->enableCsrfValidation = false;

        if(empty($_GET["key"]) || $_GET["key"] != $this->key){
            return false;
        }

        return parent::beforeAction($action);
    }

    public function actionUpdatePrices(){
        $apikey = Setting::findByKey('steamlystics_key', true);

        $response = file_get_contents('http://api.csgo.steamlytics.xyz/v2/pricelist?currency=2005&key=' . $apikey["value"]);
        //$response = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../controllers/pricelist.json');

        if(empty($response)){
            echo "Response from api server is empty";
            exit;
        }

        $response = json_decode($response, true);

        if(empty($response)){
            echo "Response from api server is incorrect";
            exit;
        }

        if(empty($response["items"])){
            echo "Items are empty";
            exit;
        }

        $items = SteamItem::find()->all();

        foreach ($items as $item) {
            if(empty($response["items"][$item->hash_name])){
                continue;
            }

            if(empty($response["items"][$item->hash_name]["safe_price"])){
                if(!empty($response["items"][$item->hash_name]["safe_net_price"])){
                    $response["items"][$item->hash_name]["safe_price"] = $response["items"][$item->hash_name]["safe_net_price"];
                }else{
                    $response["items"][$item->hash_name]["safe_price"] = 0;
                }
            }

            if($item->price != $response["items"][$item->hash_name]["safe_price"]){
                $item->price = $response["items"][$item->hash_name]["safe_price"];
                $item->save();
            }
        }
    }

    public function actionUpdateItems(){
        $apikey = Setting::findByKey('steamlystics_key', true);

        $response = file_get_contents('http://api.csgo.steamlytics.xyz/v1/items?key=' . $apikey["value"]);

        if(empty($response)){
            echo "Response from api server is empty";
            exit;
        }

        $response = json_decode($response, true);

        if(empty($response)){
            echo "Response from api server is incorrect";
            exit;
        }

        if(empty($response["items"])){
            echo "Items are empty";
            exit;
        }

        foreach ($response["items"] as $item) {
            $record = SteamItem::findOne(['hash_name' => $item['market_hash_name']]);

            if(!empty($record)){
                continue;
            }

            $record = new SteamItem();
            $record->hash_name = $item['market_hash_name'];
            $record->name = $item['market_name'];
            $record->image = $item['icon_url'];
            $record->color = $item['quality_color'];
            $record->price = 0;
            $record->save();
        }

    }
    
   
}
