<?php
namespace frontend\controllers;

include '../classes/openid.php';

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\Stat;
use common\models\CaseGroup;
use common\models\CaseCategory;
use common\models\CaseItem;
use common\models\Setting;
use common\models\SteamItem;
use common\models\Prize;
use common\models\Page;
use common\models\Faq;
use common\models\Feedback;
use common\models\Transaction;
use common\models\Dayli;
use common\models\DalyOpen;
use frontend\app\LightOpenID;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $language_dictionary;
    public $ulanguage;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'profile'],
                'rules' => [
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action){

        if(Yii::$app->controller->action->id == "check-payment"){
            $this->enableCsrfValidation = false;
        }

        if(empty(Yii::$app->request->cookies['language'])){
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => 'ru'
            ]));

            $this->ulanguage = "ru";
        }elseif(!empty($_GET["language"])){
            if($_GET["language"] == "ru"){
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => 'ru'
                ]));
                $this->ulanguage = "ru";
            }else{
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => 'en'
                ]));
                $this->ulanguage = "en";
            }
        }else{
            $this->ulanguage = Yii::$app->request->cookies['language'];
        }

        $this->language_dictionary = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/../languages/" . $this->ulanguage  . ".json");
        $this->language_dictionary = json_decode($this->language_dictionary, true);
        Yii::$app->view->params['language_dictionary'] = $this->language_dictionary;
        Yii::$app->view->params['language_prefix'] = $this->ulanguage;

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLoadDayli(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        $last_open = DalyOpen::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->one();

        if(!empty($last_open)){
            if(time() - strtotime($last_open->date) < 24*60*60){
                return json_encode(array("success" => false, "message" => $this->language_dictionary['you_opening_dayli_today']));
            }
        }

        $transactions = Transaction::find()->where(['user_id' => Yii::$app->user->id, 'success' => 1])->all();

        $summOfTransactions = 0;

        foreach ($transactions as $transaction) {
            $summOfTransactions += $transaction->summ;
        }

        $dayli = Dayli::findOne(1);

        if($summOfTransactions < $dayli->min_payment){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['for_dayli_need'] . " " . ($dayli->min_payment -  $summOfTransactions) . "р."));
        }

        $items = CaseItem::find()->where(['id' => array($dayli->item1, $dayli->item2, $dayli->item3, $dayli->item4, $dayli->item5)])->all();

        $item = new CaseItem();
        $item->title = $dayli->name_of_prize;
        $item->eng_title = $dayli->eng_name_of_prize;
        $item->chance = 100;
        $item->image = $dayli->image_of_prize;
        $item->item = 1;
        $item->category = 1;
        $item->active = 1;
        $item->sort = 1;

        $items[] = $item;

        $itemList = array();

        foreach ($items as $item) {
            $steamItem = SteamItem::findOne($item->item);

            if($this->ulanguage != "ru"){
                $item->title = $item->eng_title;
            }

            $title = !empty($item->title) ? $item->title : $steamItem->name;
            $image = !empty($item->image) ? $item->image : $steamItem->image;

            if(empty($steamItem)){
                $steamItem = new SteamItem();
                $steamItem->color = "58f33f";
            }

            $itemList[] = array("title" => $title, "image" => $image, "color" => $steamItem->color);
        }

        $items = array();

        for($i = 0; $i < 499; $i ++){
            $key = rand(0, count($itemList) - 1);
            $items[$i] = $itemList[$key];
        }

        return json_encode(array("success" => true, "items" => $items));
    }

    public function actionRollDayliCase(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        $last_open = DalyOpen::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->one();

        if(!empty($last_open)){
            if(time() - strtotime($last_open->date) < 24*60*60){
                return json_encode(array("success" => false, "message" => $this->language_dictionary['you_opening_dayli_today']));
            }
        }

        $transactions = Transaction::find()->where(['user_id' => Yii::$app->user->id, 'success' => 1])->all();

        $summOfTransactions = 0;

        foreach ($transactions as $transaction) {
            $summOfTransactions += $transaction->summ;
        }

        $dayli = Dayli::findOne(1);

        if($summOfTransactions < $dayli->min_payment){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['for_dayli_need'] . " " . ($dayli->min_payment -  $summOfTransactions) . "р."));
        }

        if($this->ulanguage != 'ru'){
            $dayli->name_of_prize = $dayli->eng_name_of_prize;
        }

        $balance = rand($dayli->min_prize, $dayli->max_prize);

        $prize = array("title" => $dayli->name_of_prize . " (" . $balance . "р.)", "image" => $dayli->image_of_prize, "color" => "58f33f");

        $user = User::findOne(Yii::$app->user->id);
        $user->balance += $balance;
        
        if($user->save()){
            $open = new DalyOpen();
            $open->user_id = $user->id;
            $open->summ_of_prize = $balance;
            $open->date = date("Y-m-d H:i:s", time());
            $open->save();
        }

        return json_encode(array("success" => true, "prize" => $prize, "balance" => number_format($user->balance, 0, "", " ")));
        
    }

    public function actionDayli(){
        $data = Dayli::findOne(1);

        return $this->render('dayli', ['data' => $data]);
    }

    public function actionSendFeedback(){
        if(Yii::$app->user->isGuest){
            $data = array("success" => fasle, "message" => $this->language_dictionary['need_auth']);
            return json_encode($data);
        }

        if(empty($_POST["feedback"])){
            $data = array("success" => false, "message" => $this->language_dictionary['empty_fields']);
            return json_encode($data);
        }

        $feedback = new Feedback();
        $feedback->user_id = Yii::$app->user->id;
        $feedback->text = $_POST["feedback"];
        $feedback->eng_text = $_POST["feedback"];
        $feedback->active = 0;

        if($feedback->save()){
            return json_encode(array("success" => true));
        }else{
            return json_encode(array("success" => false, "message" => $this->language_dictionary['error_while_save_feedback']));
        }
    }

    public function actionFeedbacks(){
        $feedbacks = Feedback::find()->where(['active' => 1])->orderBy(["id" => SORT_DESC])->asArray()->all();

        foreach ($feedbacks as &$feedback) {
            $user = User::findOne($feedback['user_id']);
            $feedback["user_name"] = $user->login;
            $feedback["user_photo"] = $user->photo;

            if($this->ulanguage != 'ru'){
                $feedback['text'] = $feedback["eng_text"];
            }
        }

        return $this->render('feedbacks', ['feedbacks' => $feedbacks]);
    }

    public function actionFaq(){
        $questions = Faq::find()->all();
        return $this->render('faq', ['questions' => $questions]);
    }

    public function actionPage($id){
        $page = Page::find()->where(['id' => $id, 'active' => 1])->one();

        if(empty($page)){
            return $this->redirect(['site/index']);
        }

        return $this->render('page', ['page' => $page]);
    }

    public function actionGeneratePayment(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        if(empty($_POST["summ"]) || (double)$_POST["summ"] < 10){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['summ_of_payment']));
        }

        $transaction = new Transaction();
        $transaction->summ = (double)$_POST["summ"];
        $transaction->user_id = Yii::$app->user->id;
        $transaction->date = date("Y-m-d H:i:s");
        $transaction->success = 0;
        $form = $transaction->generateForm();
        
        if(!$transaction->save()){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['transaction_error']));
        }

        return json_encode(array("success" => true, "form" => $form));
        return json_encode(array("success" => true, "form" => $form));
    }

     public function actionCheckPayment(){//Проверка платежа от Wallet one
        $request = Yii::$app->request->post();

        if(!isset($request["WMI_MERCHANT_ID"], $request["WMI_SIGNATURE"], $request["WMI_PAYMENT_NO"], $request["WMI_ORDER_STATE"])){
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Переданы не все POST параметры");
            return "WMI_RESULT=Retry&WMI_DESCRIPTION=Переданы не все POST параметры";
        }

        if(!empty($request["WMI_MERCHANT_ID"])){//Проверяем ID магазина с ID пришедшим в post
            $setting = Setting::findByKey("WMI_MERCHANT_ID");

            if($setting->value != $request["WMI_MERCHANT_ID"]){
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=OK");
                return "WMI_RESULT=OK";
            }
        }

        $transaction = Transaction::findOne((int)$request["WMI_PAYMENT_NO"]);

        if(empty($transaction)){//Если не найдена транзакция в БД, то выходим
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Транзакция не найдена в БД");
            return "WMI_RESULT=Retry&WMI_DESCRIPTION=Транзакция не найдена в БД";
        }

        if($request["WMI_ORDER_STATE"] != "Accepted"){//Если у статуса заказ статус отличный от успешного
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Неверный статус оплаты");
             return "WMI_RESULT=Retry&WMI_DESCRIPTION=Неверный статус оплаты";
        }

        $skey = Setting::findByKey("walletone_secret");//Получаем секретный ключ из БД
        $skey = $skey->value;

        $params = array();

        foreach($request as $name => $value){// Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE
            if ($name !== "WMI_SIGNATURE") $params[$name] = $value;
        }

        // Сортировка массива по именам ключей в порядке возрастания
        // и формирование сообщения, путем объединения значений формы

        uksort($params, "strcasecmp"); 
        $values = "";

        foreach($params as $name => $value){
            $values .= $value;
        }

        // Формирование подписи для сравнения ее с параметром WMI_SIGNATURE

        $signature = base64_encode(pack("H*", md5($values . $skey))); 

        if($signature != $request["WMI_SIGNATURE"]){//Если подписи не совпали
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Несовпадение подписей");
            return "WMI_RESULT=Retry&WMI_DESCRIPTION=Несовпадение подписей";
        }

        //ВСЕ ОК
        $transaction->success = 1;

        if(!$transaction->save()){
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Ошибка при обновлении транзакции");
            return "WMI_RESULT=Retry&WMI_DESCRIPTION=Ошибка при обновлении транзакции";
        }

        $user = User::findOne($transaction->user_id);

        $bonus_from = Setting::findByKey('payment_bonus_from')->value;
        $bonus = 0;

        if($transaction->summ >= $bonus_from){
            $bonus = Setting::findByKey('payment_bonus_summ')->value;
        }

        $user->balance += $transaction->summ + $bonus;

        if(!$user->save()){
            $transaction->success = 0;
            $transaction->save();
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=Retry&WMI_DESCRIPTION=Ошибка при обновлении баланса пользователя");
            return "WMI_RESULT=Retry&WMI_DESCRIPTION=Ошибка при обновлении баланса пользователя";
        }

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/log", "WMI_RESULT=OK&WMI_DESCRIPTION=Счет успешно пополнен");

        return "WMI_RESULT=OK&WMI_DESCRIPTION=Счет успешно пополнен";
    }

    public function actionSaveTradeLink(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        if(empty($_POST["link"])){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_trade_link']));
        }

        $user = User::findOne(Yii::$app->user->id);
        $user->trade_link = htmlspecialchars($_POST["link"]);
        $user->save();

        return json_encode(array("success" => true));
    }

    public function actionSendPrize(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        $user = User::findOne(Yii::$app->user->id);

        if(empty($user->trade_link)){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_trade_link']));
        }

        $items = Prize::find()->where(['sended' => 0, 'user_id' => $user->id])->all();

        if(empty($items)){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_out']));
        }

        foreach ($items as &$item) {
            $item->wait = 1;
            $item->save();
        }

        return json_encode(array("success" => true));
    }

    public function actionProfile(){
        $items = Prize::find()->where(['sended' => 0, 'user_id' => Yii::$app->user->id])->all();

        foreach ($items as &$item) {
            $data = CaseItem::findOne($item->item_id);
            $steamItem = SteamItem::findOne($data->item);

            if($this->ulanguage != "ru"){
                $data->title = $data->eng_title;
            }

            $title = !empty($data->title) ? $data->title : $steamItem->name;
            $image = !empty($data->image) ? $data->image : $steamItem->image;

            $item = array("title" => $title, "image" => $image, "color" => $steamItem->color);
        }

        $trade_link = Setting::findByKey('bot_trade_link');

        return $this->render('profile', ["items" => $items, "trade_link" => $trade_link->value]);
    }

    public function actionRollCase(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        if(empty($_POST["case"])){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_case_id']));
        }

        $case = CaseCategory::findOne((int)$_POST["case"]);

        if(empty($case)){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['incorrect_case_id']));
        }

        $user = User::findOne(Yii::$app->user->id);

        if($user->balance < $case['price']){
            $different = ($case['price'] - $user->balance);
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_payment']));
        }

        $items = CaseItem::find()->where(['active' => 1, 'category' => $case->id])->all();

        $prize = array();

        foreach ($items as $item) {
            $chance = $item->chance*1000;

            for($i = 0; $i < $chance; $i ++){
                $prize[] = $item->id;
            }
        }

        shuffle($prize);

        $prize = $prize[rand(0, count($prize) - 1)];

        $prize = CaseItem::findOne($prize);
        $steamItem = SteamItem::findOne($prize->item);

        if($this->ulanguage != "ru"){
            $prize->title = $prize->eng_title;
        }

        $prize = array("id" => $prize->id, "title" => (!empty($prize->title) ? $prize->title : $steamItem->name), "image" => (!empty($prize->image) ? $prize->image : $steamItem->image), "color" => $steamItem->color);

        $user->balance -= $case['price'];
        $user->save();

        $record = new Prize();
        $record->user_id = Yii::$app->user->id;
        $record->item_id = $prize["id"];
        $record->sended = 0;
        $record->wait = 0;
        $record->save();

        $stat = Stat::findOne(['type' => 'case_opened']);
        $stat->value += 1;
        $stat->save();

        return json_encode(array("success" => true, "id" => $record->id, "prize" => $prize, "balance" => number_format($user->balance, 0, "", " ")));
    }

    public function actionLoadCase(){
        if(Yii::$app->user->isGuest){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_auth']));
        }

        if(empty($_POST["case"])){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_case_id']));
        }

        $case = CaseCategory::findOne((int)$_POST["case"]);

        if(empty($case)){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['incorrect_case_id']));
        }

        $balance = User::findOne(Yii::$app->user->id)->balance;

        if($balance < $case['price']){
            $different = ($case['price'] - $balance);
            return json_encode(array("success" => false, "message" => $this->language_dictionary['need_payment']));
        }

        $items = CaseItem::find()->where(['active' => 1, 'category' => $case->id])->all();

        if(empty($items)){
            return json_encode(array("success" => false, "message" => $this->language_dictionary['empty_case']));
        }

        $itemList = array();

        foreach ($items as $item) {
            $steamItem = SteamItem::findOne($item->item);

            if($this->ulanguage != "ru"){
                $item->title = $item->eng_title;
            }

            $title = !empty($item->title) ? $item->title : $steamItem->name;
            $image = !empty($item->image) ? $item->image : $steamItem->image;

            $itemList[] = array("title" => $title, "image" => $image, "color" => $steamItem->color);
        }

        $items = array();

        for($i = 0; $i < 499; $i ++){
            $key = rand(0, count($itemList) - 1);
            $items[$i] = $itemList[$key];
        }

        return json_encode(array("success" => true, "items" => $items));
    }

    public function actionCase($id){
        $case = CaseCategory::findOne((int)$id);

        if(empty($case)){
            return $this->redirect(['site/index']);
        }

        $items = CaseItem::find()->where(['category' => $case->id])->orderBy(['sort' => SORT_ASC])->asArray()->all();

        return $this->render('case', ['id' => $case['id'], 'price' => $case['price'], 'title' => ($this->ulanguage == "ru" ? $case['title'] :  $case["eng_title"]), 'image1' => $case['image1'], 'image2' => $case['image2'], 'items' => $items]);
    }

    public function actionIndex()
    {
        $cases = array();
        $groups = CaseGroup::find()->where(['active' => 1])->orderBy(['sort' => SORT_ASC])->asArray()->all();

        foreach ($groups as $group) {
            $caseList = CaseCategory::find()->where(['active' => 1, 'group' => $group["id"]])->orderBy(['sort' => SORT_ASC])->asArray()->all();

            if(empty($caseList)){
                continue;
            }

            $cases[$group["id"]] = array("title" => ($this->ulanguage == "ru" ? $group["title"] : $group["eng_title"]), "icon" => $group["icon"], "background" => $group['background'], "cases" => array());

            foreach ($caseList as $case) {
                $itemList = CaseItem::find()->where(['active' => 1, 'category' => $case["id"]])->orderBy(['sort' => SORT_ASC])->asArray()->all();

                if(empty($itemList)){
                    continue;
                }

                $cases[$group["id"]]["cases"][$case["id"]] = array("id" => $case["id"], "price" => $case["price"], "title" => ($this->ulanguage == "ru" ? $case["title"] : $case["eng_title"]), "image1" => $case["image1"], "image2" => $case["image2"]);
            }
        }

        return $this->render('index', ["cases" => $cases]);
    }

    public function actionLogin()//Метод для стим авторизации
    {

        if (!Yii::$app->user->isGuest) {//Если уже авторизован, то шлем домой
            return $this->goHome();
        }

        $steamkey = Setting::findByKey('steam_key', true);
        try 
        {
            $_STEAMAPI = $steamkey['value'];//Steam ключ из настроек

            $openid = new LightOpenID('http://' . $_SERVER["SERVER_NAME"] . '/');

            if(!$openid->mode)//Если только зашли на страницу авторизации
            {
                if(isset($_GET['login'])) 
                {
                    $openid->identity = 'http://steamcommunity.com/openid/?l=english'; 
                    return $this->redirect($openid->authUrl(), 302);//Редиректим в стим
                }
            }
            
            if($openid->mode && $openid->mode == 'cancel') 
            {
                return $this->goHome();//Пользователь сам завершил авторизацию, шлем домой
            }
            
            if($openid->mode && $openid->validate())//Если авторизация прошла успешно
            {
                $id = $openid->identity;
                $ptn = "/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
                preg_match($ptn, $id, $matches);
 
                $url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$_STEAMAPI&steamids=$matches[1]";
                $json_object= file_get_contents($url);
                $json_decoded = json_decode($json_object);
                $player = array();
 
                foreach ($json_decoded->response->players as $player)
                {
                    $player = array(
                        "steamid" => $player->steamid,
                        "username" => $player->personaname,
                        "photo" => $player->avatarfull
                    );
                }

                $user = User::find()->where(["username" => $player["steamid"]])->one();//Ищем пользователя в БД

                $randomPasswordString = time() . rand();

                if($user == null){//Если еще нет в БД
                    $user = new User();//Создаем нового пользователя
                    $user->username = (string)$player["steamid"];
                    $user->login = $player["username"];
                    @$photo = file_get_contents($player["photo"]);

                    if(!empty($photo)){
                        $fname = "/uploads/users/" . md5(time() . $user->username) . "." . pathinfo($player["photo"])["extension"];
                        file_put_contents("../web" . $fname, $photo);
                        $photo = $fname;
                    }else{//Если не удалось загрузить фото пользователя, то ставим дефолтное
                        $photo = "/uploads/users/noavatar.png";
                    }
                    $user->photo = $photo;
                    $user->trade_link = "";
                    $user->balance = 0;
                    $user->is_real = 1;
                    $user->email = $user->username . '@steampowered.com';
                    $user->generateAuthKey();
                    $user->setPassword(md5($user->username . $randomPasswordString));
                    if(!$user->save()){
                        die("User validation error");
                    }
                }else{
                    $user->setPassword(md5($user->username . $randomPasswordString));

                    if(!$user->save()){
                        die("User validation error");
                    }
                }

                $model = new LoginForm();
                $model->username = $user->username;
                $model->password = md5($user->username . $randomPasswordString);
                $model->rememberMe = true;
                $model->login();

                return $this->goHome();//После создания сессии отправляем на главную
                
            }else{
                return $this->goHome();//Авторизация с ошибкой, шлем домой
            }
        } 
        catch(ErrorException $e) 
        {
            echo $e->getMessage();
        }
    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
