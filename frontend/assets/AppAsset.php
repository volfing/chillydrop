<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.css',
        'css/noty.css',
        'css/tooltipster.bundle.css',
        'css/tooltipster-sideTip-borderless.min.css',
        'css/main.css',
    ];
    public $js = [
        'js/jquery-3.2.1.min.js',
        'js/noty.min.js',
        'js/tooltipster.bundle.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
